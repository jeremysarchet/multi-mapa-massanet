# Mapa experimental de Maçanet de Cabrenys

[https://jeremysarchet.gitlab.io/multi-mapa-massanet](https://jeremysarchet.gitlab.io/multi-mapa-massanet)

## Components

### Git LFS

[https://git-lfs.com/](https://git-lfs.com/)

### Leaflet
- Derived from ICGC example [https://openicgc.github.io/exemples/leaflet/icgc-raster-leaflet-code.html](https://openicgc.github.io/exemples/leaflet/icgc-raster-leaflet-code.html)

### Institut Cartogràfic i Geològic de Catalunya (ICGC)
- Using the "Topogràfic clàssic" raster tiles from [https://openicgc.github.io/](https://openicgc.github.io/)

### Els Noms del Terme de Maçanet de Cabrenys (Pere Roura, 2003)
This book by local historian Pere Roura contains a quite nearly exhaustive list
of local toponyms of Maçanet and its surroundings. Descriptions from this book
used with permission.
![Els Noms del Terme de Maçanet de Cabrenys](public/images/2024-02-20-els-noms-del-terme-coberta.jpg?raw=true)


### Compendium of local landmarks and history, Pere Roura (www.massanetdecabrenys.com)
- [Les Fonts de Maçanet](https://www.massanetdecabrenys.com/lesfonts.html)
- [Camins i carreteres](https://www.massanetdecabrenys.com/camins.html)
- [Els Masos del Terme](https://www.massanetdecabrenys.com/masos.html)
- [Els Arbres Singulars i Monumentals](https://www.massanetdecabrenys.com/arbres.html)

## Other references
- [Maçanet de Cabrenys - Quaderns de la Revista de Girona - Pere Roura i Sabà](https://www.ddgi.cat/quiosc/recursos/publicacio/exemplarDigital/071-Macanet.Cabrenys.pdf)
- [Guia espais de silenci](https://xn--maanetdecabrenys-dpb.cat/wp-content/uploads/2019/04/Guia-Punts-de-silenci-DIGITAL6.pdf)
- [Fitxa inventari d'arbres singulars i d'interès no protegits de la comarca de l'alt Empordà](https://iaeden.cat/wp-content/uploads/MA%C3%87ANET-DE-CABRENYS.pdf)
- [Guia botànica i estudi integrat d’itineraris de muntanya - Gerard Carrión i Masgrau](http://www.xtec.cat/~gcarrion/Textos/Itinerari%203.%20Les%20Salines.pdf)
- [Llista de monuments de Maçanet de Cabrenys](https://ca.wikipedia.org/wiki/Llista_de_monuments_de_Ma%C3%A7anet_de_Cabrenys)
- [Llista de topònims de Maçanet de Cabrenys](https://ca.wikipedia.org/wiki/Llista_de_top%C3%B2nims_de_Ma%C3%A7anet_de_Cabrenys)
- [181 Anys de Recerca Megalítica a la Catalunya Nord (1832-2012)](https://raco.cat/index.php/AnnalsGironins/article/view/271563/359191)

### Marker pen map (Pere Roura)
![Marker pen map of Maçanet](public/images/mapa-pere-roura-1995.jpg?raw=true)
- First version: 1986, revised 1995
- Photographed from a poster paper copy, perspective corrected, desaturated,
  then colorspace reduced to 2-bit (4 possible colors: black, white, and two
  shades of gray) using ImageMagick.

### Ceramic tile map at the town hall (Joan Oller, Pere Roura)
![Ceramic map of Maçanet](public/images/ceramic-tiles/overview.jpg?raw=true)
- A 7x7 matrix of hand-painted ceramic tiles of the town limits
- Each tile hand-photographed (at an angle to reduce glare) and then perspective
corrected using ImageMagick

### Generating a tile map from a single, large raster image
The photographed ceramic tiles from the town hall, after clean-up, perspective
correction and concatenation, resulted in a single image of about 7000x7000
pixels and about a 16mb JPG at high quality setting. This proved to be too much
for performance (used as a Leaflet `ImageOverlay`) in certain situations, e.g.
on mobile. So, an alternative, perhaps fundamentally better, approach is to
take that large raster image and generate a collection of geolocated tiles
from it and then use a Leaflet `TileLayer` (with the famous`{z}/{x}/{y}` URL
pattern) instead.

#### Procedure
Here's what I did for the ceramic tiles:

Install gdal:

```
brew install gdal
```

Convert the JPEG to GeoTIFF, specifying the lat-lon bounds:

```
gdal_translate -of GTiff -a_srs EPSG:4269 -a_ullr 2.64331 42.44613 2.81572 42.32262 combined.jpg out.tif
```

Warp the GeoTIFF to Mercator:

```
gdalwarp -of GTiff  -t_srs EPSG:3857 out.tif warped.tif
```

Finally, generate the tileset:

```
python3 /opt/homebrew/lib/python3.11/site-packages/osgeo_utils/gdal2tiles.py --tiledriver=WEBP --zoom=12-18 warped.tif <output-dir>
```
Compared with PNG and JPG tilesets, the WEBP option produced the smallest size.
Perhaps there's a way to specify a lower quality for JPG using `gdal2tiles.py`.

Going from the WebP tileset to a cache-for-offline-friendly MBTiles database,
I've used `mb-util` like so. Note the `--scheme=tms` for the conversion, else
the inner-most directory numberings are changed. Also note that when setting up
the `L.tileLayer` in leaflet, we've got to specify `tms: true` in the options.

```
mb-util --image_format=webp --scheme=tms public/ceramic-tiles ceramic-webp-tms.mbtiles
```

Given a tileset in WebP format, using `mb-util` to take the tileset and create
an MBTiles database is the most straightfoward. I tried out using
`gdal_translate` with an output format of mbtiles (`-of mbtiles`), but it seems
you can't specify what format to convert the tiles to, and the PNG that it
produces gives you a *way* bigger MBTiles database.

Notes:
- The zoom argument, for development, it's faster to only specify the most
zoomed-out zoom level, i.e. `--zoom=12`
- Not using Git LFS for the tiles as they're not large files per-se, although
there are a lot of them...

#### References
- [Georeferencing using GDAL?](https://gis.stackexchange.com/a/27301)
- [gdal_translate](https://gdal.org/programs/gdal_translate.html)
- [gdal2tiles](https://gdal.org/programs/gdal2tiles.html)
- [Tiled Web Map](https://en.wikipedia.org/wiki/Tiled_web_map)

#### Other cool projects turning huge images into zoomable maps
- [Generating Map tiles at Different Zoom Levels Using Gdal2tiles in Python](https://medium.com/geekculture/generating-map-tiles-at-different-zoom-levels-using-gdal2tiles-in-python-af905eecf954)
- [How to Make an Interactive Story Map Using Leaflet and Non-Geographical Images](https://jarednielsen.medium.com/how-to-make-an-interactive-story-map-using-leaflet-and-non-geographical-images-821f49ff3b0d)
- [Pictorial St. Louis](http://jarednielsen.com/pictorial-st-louis/index.html)
- [leaflet-rastercoords](https://commenthol.github.io/leaflet-rastercoords/)
- [Showing Zoomify images with Leaflet](https://blog.mastermaps.com/2013/06/showing-zoomify-images-with-leaflet.html)
- [gdal2tiles-leaflet](https://github.com/commenthol/gdal2tiles-leaflet)

## Offline functionality

### Downloading ICGC topo tiles in set bounds

I've used the [Map Tiles Crawler](https://github.com/stefanhuber/map-tiles-crawler)
tool. See `tools/tile-crawl.sh`.

### Compressing ICGC tiles

The crawled topo tiles within the bounds of the ceramic tiles results in a
tree of PNGs totalling ~200MB.

A friend suggested I look into compressing them by lowering the quality
setting, converting to JPG, or converting to GIF and limiting the color
palette. An initial test with Photoshop seemed promising, but further tests on
the tree of crawled tiles were disappointing. In the end I was happiest with
the results from using [pngquant](https://pngquant.org/).

#### pngquant

I've found the results of `pngquant` to be the best of all the compression
options I tested.  With `pngquant` at a low quality (e.g. 10) the tileset is
compressed to about 2/3 its original size. With JPEG compression, to get it
down to a comparable size you end up with significant artifacts across solid
regions of colors. I guess what's going on is that JPEG compression is designed
for photographic images, without solid regions of colors, and PNG better
  respects solid regions of color.

Note: normally, `pngquant` creates a separate file as output with a distinct
suffix.  But if you explicitly specify the extension to be the same as that of
the input file and you force overwrite, then it will overwrite the input image.

```
pngquant --quality=5 --ext '.png' -f test.png
```

```
find ./ -name "*.png" -exec pngquant --quality=5 --ext '.png' -f {} \;
```

#### Other approach tested: palettizing image colors with ImageMagick
- [Extracting Image Colors (ImageMagick docs)](https://legacy.imagemagick.org/Usage/quantize/#extract)
- [Dither using Pre-Defined Color Maps (ImageMagick docs)](https://legacy.imagemagick.org/Usage/quantize/#remap)

Reduce an image's colors to 16 and output them to a 1x16 pixel image:

```
convert in.png -colors 16 -unique-colors out.gif
```

The same, but with 5 colors and scaled way up so it's easier to preview:

```
convert in.png -colors 5 -unique-colors -scale 1000%  out.gif
```

Additional references:

- [Floyd Steinberg dithering in GraphicsMagic or ImageMagic](https://stackoverflow.com/questions/34729404/floyd-steinberg-dithering-in-graphicsmagic-or-imagemagic)
- [Remap / posterize image colors using list of colors in imagemagick](https://stackoverflow.com/questions/25253247/remap-posterize-image-colors-using-list-of-colors-in-imagemagick)

I wasn't so happy with the results of going straight a low number of colors,
e.g. 16. So another thing I tried was first going to a medium number, e.g. 128,
and then feeding that palette itself as a source to get 16. This looked nicer
but still unsatisfactory.

```
convert in-tile.png -colors 128 -unique-colors palette-128.png

convert palette-128.png -colors 16 -unique-colors palette-128-16.png

convert in-tile.png -remap palette-128-16.png out.png
```

Create a palette manually:

- [How can I convert a list of hex colors (or RGB, sRGB) stored in a text file into an Image](https://stackoverflow.com/questions/38477123/how-can-i-convert-a-list-of-hex-colors-or-rgb-srgb-stored-in-a-text-file-into)

In-line stipulation of colors in the palette:

```
convert -size 1x1 xc:'#FF0000' xc:'#00FF00' xc:'#0000FF' -append -rotate '-90' p.png
```

Palette colors defined in a text file:

```
while read h; do convert xc:"$h" miff:- ; done < palette.txt | convert -append miff:- result.png
```


#### MBTiles

Using [MBUtil](https://github.com/mapbox/mbutil/), we can take a file tree
(a tileset) and turn it into an MBTiles file like so:

```
mb-util topo-tiles topo.mbtiles
```

- [How to convert a PNG tile set into the Mbtiles format](https://gis.stackexchange.com/questions/90687/how-to-convert-a-png-tile-set-into-the-mbtiles-format)
- [Generating .mbtiles from an image](https://mapitgis.com/generating-mbtiles-from-an-image/)
- [Leaflet.TileLayer.MBTiles](https://gitlab.com/IvanSanchez/Leaflet.TileLayer.MBTiles/-/tree/master/)
- [Ideas for an alternative to mbtiles for map tile storage](https://gist.github.com/gmaclennan/7c5cee11422412752cd0e6699f108641)
- [MBTiles.js Pure JavaScript MBTiles parser](https://github.com/tilemapjp/mbtiles.js)
- [Offline Map Tiles without PhoneGap or mbtiles](https://github.com/Leaflet/Leaflet/issues/1829)
- [Exporting rasters to Mbtiles using GDAL](https://pvanb.wordpress.com/2017/03/06/raster2mbtiles/)
- [Convert an image (like JPG or TIF) to MBTiles using GDAL](https://gist.github.com/jbaranski/0073f7b98bdf1f64f49988853daed67b)
- [Generating MBTiles files with custom imagery](https://vespucci.io/tutorials/custom_imagery_mbtiles/)
- [gdal2mbtiles](https://github.com/ecometrica/gdal2mbtiles)
- [How do I create offline layers using MBTiles?](https://help.fulcrumapp.com/en/articles/4351972-how-do-i-create-offline-layers-using-mbtiles)
- [MBTiles Specification](https://github.com/mapbox/mbtiles-spec)
- [How do I create offline layers using MBTiles?](https://help.fulcrumapp.com/en/articles/4351972-how-do-i-create-offline-layers-using-mbtiles)
- [Post processing MBTiles with MBPipe](https://github.com/mapbox/node-mbtiles/wiki/Post-processing-MBTiles-with-MBPipe)

#### Offline maps
- [Offline maps (Mapbox docs)](https://docs.mapbox.com/help/troubleshooting/mobile-offline/)
- [Offline - Maps SDK for Android (Mapbox docs)](https://docs.mapbox.com/android/maps/guides/offline/)
- [Progressive Web Maps (PWA)](https://github.com/reyemtm/pwa-maps)
- [Leaflet Offline Map: How to store a map for offline usage?](https://stackoverflow.com/questions/62989519/leaflet-offline-map-how-to-store-a-map-for-offline-usage)
- [Rendering leaflet.js tiles in the browser](http://richorama.github.io/2018/08/22/rendering-leaflet-tiles-in-the-browser/)
- [Offline Maps for Mobile or Tablet](http://sebastian-meier.github.io/OfflineMaps/)
- [Do I need to create native app to use leaflet map offline](https://stackoverflow.com/questions/23971274/do-i-need-to-create-native-app-to-use-leaflet-map-offline)
- [Offline Map examples in web browser](https://github.com/tbicr/OfflineMap)
- [Database for offline slippy map tiles](https://gis.stackexchange.com/questions/44813/database-for-offline-slippy-map-tiles)
- [Storing Image Data for offline web application (client-side storage database)](https://stackoverflow.com/questions/14113278/storing-image-data-for-offline-web-application-client-side-storage-database)
- [pouchdb](https://pouchdb.com/)
- [Phonegap + Leaflet + TileMill = Offline Mobile Maps](http://geospatialscott.blogspot.com/2012/04/phonegap-leaflet-tilemill-offline.html)
- [OfflineMap](http://tbicr.github.io/OfflineMap/)
- [Using PouchDB as an offline raster map cache](https://stackoverflow.com/questions/16721312/using-pouchdb-as-an-offline-raster-map-cache)
- [Landez](https://github.com/makinacorpus/landez)
- [SWOM - Service Worker Offline Map](https://github.com/WebReflection/map)
- [Publishing Your MapApp](https://medium.com/runic-software/publishing-your-mapapp-229ba1e8258c)

#### Progressive web apps (PWAs)
- [The offline cookbook](https://jakearchibald.com/2014/offline-cookbook/)
- [How to provide your own in-app install experience](https://web.dev/customize-install/)
- [Progressive Web Apps: Escaping Tabs Without Losing Our Soul](https://infrequently.org/2015/06/progressive-apps-escaping-tabs-without-losing-our-soul/)
- [Installing and uninstalling web apps (MDN docs)](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Installing)
- [Add to Home screen (MDN docs)](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Add_to_home_screen)
- [Is there a way to force a PWA to stay in offline mode via a toggle in-app?](https://stackoverflow.com/questions/61865712/is-there-a-way-to-force-a-pwa-to-stay-in-offline-mode-via-a-toggle-in-app)
- [Storage for the web (web.dev)](https://web.dev/storage-for-the-web/)
- [Caching (web.dev)](https://web.dev/learn/pwa/caching/)
- [Browser Storage Abuser](https://demo.agektmr.com/storage/)
- [Fetch progress indicators](https://github.com/AnthumChris/fetch-progress-indicators)

#### Image formats
- [Which format for small website images? GIF or PNG?](https://stackoverflow.com/questions/115818/which-format-for-small-website-images-gif-or-png)

## Other

### Recursively convert images

- [Recursive converting multiple files from PNG to JPG](https://mensfeld.pl/2012/09/recursive-converting-multiple-files-from-png-to-jpg/)

`find ./ -name "*.png" -exec mogrify -format jpg {} \;`

`find ./ -name "*.png" -exec rm {} \;`

### Recursively count files in directory

- [Recursively counting files in a Linux directory](https://stackoverflow.com/a/9157162/15487978)
`find <dir> -type f | wc -l`

### Paper on compressing raster tiles

- [Topographic Map Visualization from Adaptively Compressed Textures](https://upcommons.upc.edu/bitstream/handle/2117/15705/Topographic_map.pdf?sequence=1)

### Tar

Create gzip archive:

```
tar cvzf topo-tiles.tar.gz topo-tiles/
```

Unpack archive:
```
tar xvfz assets/ceramic-tiles/ceramic-tiles.tar.gz --directory public;
```

## Other cool projects

- [Gas Mountain](https://gasmountain.com/)
- [CercaFonts (ICGC)](https://www.icgc.cat/Ciutada/Destacats/Aplicacions-mobils/CercaFonts)
- [Wikipedra](http://wikipedra.catpaisatge.net/)
- [Mapa de la Linea P](https://lineap.spiki.org/)
