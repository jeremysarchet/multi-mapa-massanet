#!/usr/bin/env bash

rm -rf public/marcadors
mkdir public/marcadors
rm -rf public/conjunts
mkdir public/conjunts
cp src/landmark-details.css public/marcadors
cp src/utils.mjs public/utils.mjs

# For the "online" ceramic tiles, unzip and place tileset in `public`
if [[ -n $GITLAB_CI ]]; then
  rm -rf ./public/ceramic-tiles;

  tar xvfz assets/ceramic-tiles/ceramic-tiles.tar.gz --directory public;
fi

node src/build.mjs
