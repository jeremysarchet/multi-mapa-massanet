'use strict'

import { BUCKET_BASE_URL, createSlug } from "./utils.mjs";

/*
 * TODO:
 *
 * - Look into OCR for the ICGC topo tiles
 * - Figure out why orientation doesn't update upon device rotate in Android PWA
 * - Why GPS location needs a kick sometimes
 *
 */

const MAP_BOUNDS = [
  [42.48780, 2.53681],
  [42.29153, 2.93884],
];

const CERAMIC_BOUNDS = [
  [42.44613, 2.64331],
  [42.32262, 2.81572],
];

const MARKER_PEN_BOUNDS = [
  [42.46030, 2.61024],
  [42.32881, 2.81868]
];

const INITIAL_ZOOM = 13;

const MAP_BOX_ACCESS_TOKEN = 'pk.eyJ1IjoianNhcmNoZXQiLCJhIjoiY2xnZHY3N3BuMGRseDNnbWZwcW5xdnRueSJ9.RBl8mBgzS5lZ-GyPMpwy8g';
const MAP_BOX_URL_PATTERN = `https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${MAP_BOX_ACCESS_TOKEN}`;
const MAP_BOX_ATTRIBUTION = `<a target="_blank" href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>
  col·laboradors,
`;

const ICGC_URL_PATTERN = 'https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/{z}/{x}/{y}.png';
const ICGC_ATTRIBUTION = `<a target="_blank" href="https://openicgc.github.io/">ICGC</a>`;

const OSM_URL_PATTERN = 'https://tile.openstreetmap.org/{z}/{x}/{y}.png';
const OSM_ATTRIBUTION = `<a target="_blank" href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>`;

const OPEN_TOPO_MAP_URL_PATTERN = 'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png';
const OPEN_TOPO_MAP_ATTRIBUTION = '<a target="_blank" href="https://opentopomap.org/credits">OpenTopoMap</a>';

const CERAMIC_ATTRIBUTION = `<a target="_blank" href="https://www.massanetdecabrenys.com/">Pere Roura</a>,
  Joan Oller`;

const MARKER_PEN_IMAGE_URL = 'images/mapa-pere-roura-1995.jpg';
const CERAMIC_TILES_URL_PATTERN = 'ceramic-tiles/{z}/{x}/{y}.webp';

// A few global map entities so they can be manipulated just like DOM elements
let map;
let layerControl;
let icgcTopoBaseLayer;
let icgcTopoOfflineBaseLayer;
let ceramicOfflineBaseLayer;
let openStreetMapBaseLayer;
let openTopoMapBaseLayer;
let zoomIndicator;

let detailMBTilesLoading;
let detailMBTilesProgress;
let detailMBTilesCached;
let ceramicMBTilesLoading;
let ceramicMBTilesCached;
let ceramicMBTilesProgress;

// Functionality to "fly to" a new marker based on the URL hash. See the
// `window.onhashchange` below. Currently provides nice functionality for
// when you cleck a "see also" link in a popover, you fly to it.
const landmarkEntities = new Map();

// https://stackoverflow.com/a/20732091/15487978
function humanizeFileSize(size) {
    var i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
    return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

L.Control.Layers.include({
  // https://stackoverflow.com/a/51484131/15487978
  getLayers: function() {
    const result = { baseLayers: {}, overlays: {} };

    this._layers.forEach((obj) => {
      const layerEnabled = this._map.hasLayer(obj.layer);

      if (obj.overlay) {
        result.overlays[obj.name] = layerEnabled;
      } else {
        // A casual inspection seems to indicate if `overlay` is `undefined`
        // then we have a base layer
        result.baseLayers[obj.name] = layerEnabled;
      }
    });

    return result;
  },
  hasBaseLayer: function(layerName) {
    const { baseLayers, _ } = this.getLayers();
    return layerName in baseLayers;
  },
  isBaseLayerEnabled: function(layerName) {
    const { baseLayers, _ } = this.getLayers();
    return layerName in baseLayers && baseLayers[layerName] === true;
  },
  hasOverlay: function(layerName) {
    const { _, overlays } = this.getLayers();
    return layerName in overlays;
  }
});

async function updateStorageReport() {
  const cacheKeys = await caches.keys();

  const storageReport = document.getElementById('storage');
  storageReport.innerText = 'Storage:\n  unknown';

  if ('storage' in navigator && 'estimate' in navigator.storage) {
    // https://developer.mozilla.org/en-US/docs/Web/API/StorageManager/estimate
    const { usage, quota } = await navigator.storage.estimate();
    storageReport.innerText = 'Storage:\n';
    storageReport.innerText += `  quota: ${humanizeFileSize(quota)}\n  usage: ${humanizeFileSize(usage)}`;
  }

  // Ascertain the stats for storage capacity and usage
  const cacheInfo = document.getElementById('caches');
  cacheInfo.innerText = 'Caches:\n';
  cacheKeys.forEach(async (cacheKey) => {
    // https://developer.mozilla.org/en-US/docs/Web/API/Cache/keys
    const cache = await caches.open(cacheKey);
    const keys = await cache.keys()
    cacheInfo.innerText += `  - ${cacheKey} (${keys.length} files)\n`;
  })

  const OFFLINE_TOPO_LAYER_NAME = 'ICGC Topo (offline)';

  caches.match(TOPO_14_18_MBTILES_PATH).then((response) => {
    const missingTopoMBTiles = response === undefined;
    detailMBTilesCached.hidden = missingTopoMBTiles;

    if (layerControl && icgcTopoOfflineBaseLayer) {
      if (!missingTopoMBTiles) {
        if (!layerControl.hasBaseLayer(OFFLINE_TOPO_LAYER_NAME)) {
          //console.log('Adding the offline topo to controls..')
          layerControl.addBaseLayer(icgcTopoOfflineBaseLayer, OFFLINE_TOPO_LAYER_NAME);
        } else {
          //console.log('No need to add the offline topo to controls; it is already there');
        }
      } else {
        //console.log('About to remove the offline topo from controls.');

        if (layerControl.isBaseLayerEnabled(OFFLINE_TOPO_LAYER_NAME)) {
          //console.log('Offline topo base layer currently enabled, got to switch to another layer first');
          map.addLayer(openStreetMapBaseLayer);
          map.removeLayer(icgcTopoOfflineBaseLayer);
        }
        //console.log('Removing the offline topo from map controls....')
        layerControl.removeLayer(icgcTopoOfflineBaseLayer);
      }
    }
  });

  const OFFLINE_CERAMIC_LAYER_NAME = 'Rajoles (offline)';

  caches.match(CERAMIC_MBTILES_PATH).then((response) => {
    const missingCeramicMBTiles = response === undefined;
    ceramicMBTilesCached.hidden = missingCeramicMBTiles;

    if (layerControl && ceramicOfflineBaseLayer) {
      if (!missingCeramicMBTiles) {
        if (!layerControl.hasBaseLayer(OFFLINE_CERAMIC_LAYER_NAME)) {
          //console.log('Adding the offline ceramic tiles to controls..')
          layerControl.addBaseLayer(ceramicOfflineBaseLayer, OFFLINE_CERAMIC_LAYER_NAME);
        } else {
          //console.log('No need to add the offline ceramic tiles to controls; it is already there');
        }
      } else {
        //console.log('About to remove the offline ceramic tiles from controls.');

        if (layerControl.isBaseLayerEnabled(OFFLINE_CERAMIC_LAYER_NAME)) {
          //console.log('Offline ceramic tiles base layer currently enabled, got to switch to another layer first');
          map.addLayer(openStreetMapBaseLayer);
          map.removeLayer(ceramicOfflineBaseLayer);
        }
        //console.log('Removing the offline ceramic tiles from controls....')
        layerControl.removeLayer(ceramicOfflineBaseLayer);
      }
    }
  });
}

function excerpt(text) {
  const firstParagraph = text.split('\n\n')[0];
  return '<p>' + firstParagraph+ '</p>'
}

// https://stackoverflow.com/a/20732091/15487978
function hfs(size) {
    var i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
    return (size / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

function init() {
  const activeLandmarkSlug = window.location.hash.split('#')[1];

  if (activeLandmarkSlug) {
    document.getElementById('modal-container').hidden = true;
  }

  // Initialization of global variables
  detailMBTilesLoading = document.getElementById('detail-mbtiles-fetching');
  detailMBTilesProgress = document.getElementById('detail-mbtiles-progress');
  detailMBTilesCached = document.getElementById('detail-mbtiles-cached');
  ceramicMBTilesLoading = document.getElementById('ceramic-mbtiles-fetching');
  ceramicMBTilesCached = document.getElementById('ceramic-mbtiles-cached');
  ceramicMBTilesProgress = document.getElementById('ceramic-mbtiles-progress');

  // Initialization of global variable
  map = L.map('map', {
    center: [42.39, 2.73],
    zoom: INITIAL_ZOOM,
    minZoom: 12,
    // Max zoom seems to default to 18
  });

  L.control.scale({ metric: true, imperial: false }).addTo(map);

  L.Control.ZoomIndicator = L.Control.extend({
    options: {
      position: 'bottomleft',
    },
    onAdd: function (map) {
      const zoomIndicatorWrapper = L.DomUtil.create('div', 'leaflet-control leaflet-control-scale');

      // Setting value of a global variable
      zoomIndicator = L.DomUtil.create('div', 'leaflet-control-scale-line zoom-indicator', zoomIndicatorWrapper);
      zoomIndicator.innerText = INITIAL_ZOOM;

      return zoomIndicatorWrapper;
    }
  });

  map.addControl(new L.Control.ZoomIndicator());

  L.control.locate({ showCompass: true }).addTo(map);

  map.attributionControl.addAttribution(CERAMIC_ATTRIBUTION);

  // Initialization of global variable
  icgcTopoBaseLayer = L.tileLayer(ICGC_URL_PATTERN, {
    bounds: MAP_BOUNDS,
    attribution: ICGC_ATTRIBUTION,
  });

  // Initialization of global variable
  icgcTopoOfflineBaseLayer = L.tileLayer.mbTiles(TOPO_14_18_MBTILES_PATH, {
    bounds: CERAMIC_BOUNDS,
    attribution: ICGC_ATTRIBUTION,
  });

  // Initialization of global variable
  ceramicOfflineBaseLayer = L.tileLayer.mbTiles(CERAMIC_MBTILES_PATH, {
    tms: true,
    bounds: CERAMIC_BOUNDS,
    attribution: CERAMIC_ATTRIBUTION,
  });

  const satelliteBaseLayer = L.tileLayer(MAP_BOX_URL_PATTERN, {
    bounds: MAP_BOUNDS,
    attribution: MAP_BOX_ATTRIBUTION,
    id: 'mapbox/satellite-v9',
    tileSize: 512,
    zoomOffset: -1,
  });

  // Initialization of global variable
  openStreetMapBaseLayer = L.tileLayer(OSM_URL_PATTERN, {
    bounds: MAP_BOUNDS,
    attribution: OSM_ATTRIBUTION,
  });

  // Initialization of global variable
  openTopoMapBaseLayer = L.tileLayer(OPEN_TOPO_MAP_URL_PATTERN, {
    bounds: MAP_BOUNDS,
    attribution: OPEN_TOPO_MAP_ATTRIBUTION,
  });

  var markerPenBaseLayer = L.layerGroup();
  var markerPenOverlay = L.imageOverlay(
    MARKER_PEN_IMAGE_URL,
    MARKER_PEN_BOUNDS,
    {
      opacity: 1.0,
    }
  );
  markerPenBaseLayer.addLayer(markerPenOverlay);

  /* The ceramic tiles were initially implemented as an `imageOverlay`. This
   * led to performance issues as the single heavy image is a lot to ask in
   * certain situations, e.g. on mobile. Now the ceramic tiles are implemented
   * using an actual `tileLayer`, with tiles generated from the single source
   * image. Keeping the below here for reference. The use of the `imageOverlay`
   * worked well for the task of determining the upper-left and lower-right
   * lat-lon coordinates of the source image.
   *
      var ceramicTilesGroup = L.layerGroup();
      var overlay = L.imageOverlay(
        CERAMIC_TILES_IMAGE_URL,
        CERAMIC_BOUNDS,
        {
          opacity: 1.0,
        }
      );
      ceramicTilesGroup.addLayer(overlay);
   */
  var ceramicTilesBaseLayer = L.tileLayer(CERAMIC_TILES_URL_PATTERN, {
    tms: true,
    bounds: CERAMIC_BOUNDS,
    attribution: CERAMIC_ATTRIBUTION,
  });

  // Right click to see coordinates
  map.addEventListener('contextmenu', (event) => {
    const locationString = `[${event.latlng.lat.toFixed(5)}, ${event.latlng.lng.toFixed(5)}]`;
    alert("Coordenades: " + locationString);
    console.log(`location: ${locationString}`);
  });

  map.on('zoomend', () => {
    zoomIndicator.innerText = map.getZoom();
  });

  const ExtendedIcon = L.Icon.Default.extend({ options: {}});

  function popupContent(thing, slug, landmark, trackStats) {
    const description = landmark.description ? `
      <div class='description ${thing}-description'>
        ${Array.isArray(landmark.description)
            ? excerpt(landmark.description[0].value)
            : excerpt(landmark.description.value)}
      </div>
    ` : '';

    const featuredPhoto = landmark.photos ? `
      <img class="popup-photo" src="${BUCKET_BASE_URL}/600/${landmark.photos[0]}.600.jpg" />
    ` : '';

    const seeAlsoLinks = landmark.see_also ? `
      <hr />
      <h4>Vegeu també:</h4>
      ${landmark.see_also.map((landmarkName) =>
        `<p><a href="#${createSlug(landmarkName)}">${landmarkName}</a></p>`
      ).join('\n')}` : '';

    return `
      <div style="margin-bottom: 1em">
        <h3 style="margin-bottom: 0">${landmark.name}</h3>
        ${landmark.alternateNames ?
          `<em>${landmark.alternateNames.join(', ')}</em>` :
        ''}
      </div>
      ${(landmark.groups && landmark.groups[0]) ?
        `<em>${landmark.groups[0].section}</em>` : ''}
      ${(landmark.location?.unsure || landmark.track?.unsure) ?
        '<p style="color: gray">⚠️ Geolocalització aproximada</p>' : ''}
      ${trackStats ?
        `<p><span style="font-family: monospace">Distància: ${trackStats.distanceKilometers.toFixed(1)} km</span></p>`
        : ''
      }
      ${featuredPhoto}
      ${description}
      <a href="./marcadors/${slug}.html">Més detalls</a>
      ${seeAlsoLinks}
    `;
  }

  function landmarkGroup(thing) {
    const landmarkIcon = new ExtendedIcon({ className: `${thing}-icon` });
    const unsureLandmarkIcon = new ExtendedIcon({ className: `unsure ${thing}-icon` });
    var group = L.layerGroup();
    const url = `landmarks/${thing}s.yaml`;
    fetch(url)
    .then((response) => {
      if (!response.ok) {
        console.error(`Bad response fetching ${url}`);
        return group;
      }
      return response.text()
    })
    .then((data) => {
      let count = 0;
      let undefinedPlacementCount = 0;
      jsyaml.loadAll(data, (landmark) => {
        if (!(landmark.location || landmark.track)) {
          undefinedPlacementCount ++;
          return;
        }
        const slug = createSlug(landmark.name);

        if (landmark.location) {
          const marker = L.marker(
            landmark.location.value,
            {
              icon: landmark.location.unsure ? unsureLandmarkIcon : landmarkIcon
            }
          );
          marker.bindPopup(popupContent(thing, slug, landmark));
          group.addLayer(marker);

          // Start out showing this one based on the URL hash
          if (activeLandmarkSlug === slug) {
            group.addTo(map);
            map.setView(landmark.location.value, 18);
            marker.openPopup();
          }
          landmarkEntities.set(slug, { landmark, group, marker });
        }

        if (landmark.track) {
          const trail = new L.GPX(
            `tracks/${landmark.track.file}`,
            {
              async: true,
              marker_options: {
                startIcon: new ExtendedIcon({ className: 'track-start-icon' }),
                endIcon: new ExtendedIcon({ className: 'track-end-icon' }),
                shadowUrl: 'scripts/leaflet/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                shadowSize: [41, 41],
                shadowAnchor: [12, 41],
              },
              polyline_options: {
                color: 'coral',
                weight: '5'
              }
            }
          );
          group.addLayer(trail);

          trail.on('addpoint', function(e) {
            //console.log('Added ' + e.point_type + ' point: ', e.point);
          })

          // https://plnkr.co/edit/NO2acQlJPjnyQ3cF9qqW?preview
          trail.on('loaded', function(e) {
            const gpx = e.target;
            const trackStats = {
              trackName: gpx.get_name(),
              distanceMeters: gpx.get_distance(),
              distanceKilometers: gpx.get_distance() / 1000,
              elevationGain: gpx.get_elevation_gain().toFixed(0),
              elevationLoss: gpx.get_elevation_loss().toFixed(0),
            }

            // register popup on click
            console.log('layers', gpx.getLayers());
            const polyLine = gpx.getLayers()[0];
            console.log('polyLine', polyLine);
            polyLine.bindPopup(popupContent(thing, slug, landmark, trackStats))

            // Start out showing this one based on the URL hash
            if (activeLandmarkSlug === slug) {
              group.addTo(map);
              map.fitBounds(polyLine.getBounds());
              polyLine.openPopup();
            }
            landmarkEntities.set(slug, { landmark, group, polyLine });
          });
        }

        count ++;
      });
      const undefinedCountMessage = `(${undefinedPlacementCount} with unspecified location/track not loaded)`;
      console.log(`Loaded ${count} ${thing}s. ${
        undefinedPlacementCount ? undefinedCountMessage : ''
      }`);
    }).catch((error) => {
      console.error(`Something went wrong with landmark group: ${thing}.`, error);
      return group;
    });
    return group;
  }

	const baseLayers = {
		'ICGC Topo': icgcTopoBaseLayer,
    'Rotulador': markerPenBaseLayer,
    'Rajoles': ceramicTilesBaseLayer,
		'OpenStreetMap': openStreetMapBaseLayer,
		'OpenTopoMap': openTopoMapBaseLayer,
    'Satèl·lit': satelliteBaseLayer,
	};

  const farmhousesGroup = landmarkGroup('farmhouse');

  var overlays = {
    'Fonts': landmarkGroup('fountain'),
    'Masos': farmhousesGroup,
    'Masos desapareguts i casals': landmarkGroup('nonextant-farmhouse'),
    'Cases': landmarkGroup('house'),
    'Arbres': landmarkGroup('tree'),
    'Ponts': landmarkGroup('bridge'),
    'Collades': landmarkGroup('saddle'),
    'Camps, prats, boscos': landmarkGroup('field'),
    //'Gorgs, gorgues, cascades': landmarkGroup('pool'),
    'Mines, pedreres': landmarkGroup('mine'),
    'Monuments': landmarkGroup('monument'),
    'Cims, muntanyes': landmarkGroup('peak'),
    'Castells, forts': landmarkGroup('fort'),
    'Comerç': landmarkGroup('mill'),
    'Esglésies, capelles': landmarkGroup('chapel'),
    'Coves': landmarkGroup('cave'),
    'Camins, carreteres': landmarkGroup('road'),
  }

  // Initialization of global variable
  layerControl = L.control.layers(baseLayers, overlays).addTo(map);

  // Start out with the ICGC online layer selected
  //map.addLayer(openStreetMapBaseLayer);
  map.addLayer(icgcTopoBaseLayer);

  if (!activeLandmarkSlug) {
    // Start out with the farmhouses layer selected
    farmhousesGroup.addTo(map);
  }

  // Test for GPX
  /*
  const trail = new L.GPX(
    'tracks/test.gpx',
    {
      async: true,
      marker_options: {
        startIcon: new ExtendedIcon({ className: `track-start-icon` }),
        endIcon: new ExtendedIcon({ className: `track-end-icon` }),
        shadowUrl: 'scripts/leaflet/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        shadowSize: [41, 41],
        shadowAnchor: [12, 41],
      },
      polyline_options: {
        color: 'cornflowerblue',
        weight: '5'
      }
    }
  );
  trail.addTo(map);

  trail.on('addpoint', function(e) {
    console.log('Added ' + e.point_type + ' point: ', e.point);
  })

  // https://plnkr.co/edit/NO2acQlJPjnyQ3cF9qqW?preview
  trail.on('loaded', function(e) {
    var gpx = e.target,
      name = gpx.get_name(),
      distM = gpx.get_distance(),
      distKm = distM / 1000,
      distKmRnd = distKm.toFixed(1),
      eleGain = gpx.get_elevation_gain().toFixed(0),
      eleLoss = gpx.get_elevation_loss().toFixed(0);
    var info = "Name: " + name + "</br>" +
    "Distance: " + distKmRnd + " km </br>" +
    "Elevation Gain: " + eleGain + " m </br>"

    // register popup on click
    gpx.getLayers()[0].bindPopup(info)
  });
  */


  const modalContainer = document.getElementById('modal-container');
  const infoContent = document.getElementById('info');
  const settingsContent = document.getElementById('settings');

  function showInfo() {
    modalContainer.hidden = false;
    infoContent.hidden = false;
    settingsContent.hidden = true;
  }

  async function showSettings() {
    await updateStorageReport();
    modalContainer.hidden = false;
    infoContent.hidden = true;
    settingsContent.hidden = false;
  }

  // https://gis.stackexchange.com/a/111744
  L.Control.Info = L.Control.extend({
    options: {
      position: 'bottomright',
    },
    onAdd: function (map) {
      const controlDiv = L.DomUtil.create('div', 'leaflet-draw-toolbar leaflet-bar');

      const infoButton = L.DomUtil.create('a', 'control leaflet-draw-edit-remove', controlDiv);
      infoButton.title = 'Info';
      infoButton.href = '#';
      infoButton.innerText = 'ℹ';

      L.DomEvent
        .addListener(infoButton, 'click', L.DomEvent.stopPropagation)
        .addListener(infoButton, 'click', L.DomEvent.preventDefault)
        .addListener(infoButton, 'click', showInfo);

      const settingsButton = L.DomUtil.create('a', 'control leaflet-draw-edit-remove', controlDiv);
      settingsButton.title = 'Configuració';
      settingsButton.href = '#';
      settingsButton.innerText = '⚙';

      L.DomEvent
        .addListener(settingsButton, 'click', L.DomEvent.stopPropagation)
        .addListener(settingsButton, 'click', L.DomEvent.preventDefault)
        .addListener(settingsButton, 'click', showSettings);

      return controlDiv;
    }
  });

  const infoControl = new L.Control.Info();
  map.addControl(infoControl);


  document.getElementById('modal-container').onclick = function(e) {
      if (e.srcElement.id === 'modal-container') {
      document.getElementById('modal-container').hidden = true;
    }
  }

  document.getElementById('modal-close-button').onclick = function(e) {
    document.getElementById('modal-container').hidden = true;
  }

  async function clearAll() {
    const cacheKeys = await caches.keys()

    await Promise.all(cacheKeys.map((key) => caches.delete(key)));

    const registrations = await navigator.serviceWorker.getRegistrations()

    await Promise.all(registrations.map((registration) => registration.unregister()));

    updateStorageReport();
    location.reload();
  }

  document.getElementById('clear').onclick = function(e) {
    clearAll();
  }

  /* Report fetch progress by setting `innerText` on the given DOM element
   *
   * NOTE: Very excited for fetch progress!
   *
   * Adapted from:
   * https://github.com/anthumchris/fetch-progress-indicators/blob/master/fetch-basic/supported-browser.js
   *
   */
  function trackFetchProgress({ clonedResponse, statusElement }) {
    statusElement.innerText = '0%';
    const contentLength = clonedResponse.headers.get('content-length');
    const total = parseInt(contentLength, 10);
    let loaded = 0;

    new Response(
      new ReadableStream({
        start(controller) {
          const reader = clonedResponse.body.getReader();

          read();
          function read() {
            reader.read().then(({done, value}) => {
              if (done) {
                controller.close();
                return;
              }
              loaded += value.byteLength;
              statusElement.innerText = Math.round(loaded/total*100)+'%';
              controller.enqueue(value);
              read();
            }).catch(error => {
              console.error(error);
              controller.error(error)
            })
          }
        }
      })
    );
  }

  document.getElementById('detail-mbtiles-download-button').onclick = function(e) {
    detailMBTilesLoading.hidden = false;
    detailMBTilesProgress.hidden = false;
    detailMBTilesCached.hidden = true;

    // TODO: Remember to update the cache version if/when the MBTiles file is updated
    caches.open("icgc-mbtiles-zoom-levels-12-18-v1")
    .then((cache) => {
      fetch(TOPO_14_18_MBTILES_PATH)
      .then((response) => {
        // Clone the response for "measurement" use
        // https://jakearchibald.com/2014/reading-responses/
        const clonedResponse = response.clone();

        trackFetchProgress({ clonedResponse, statusElement: detailMBTilesProgress });

        if (!response.ok) {
          alert(`Failed to download offline ICGC topo map. (${response.status} ${response.statusText})`);
          console.error(response);
          return
        }
        return cache.put(TOPO_14_18_MBTILES_PATH, response);
      })
      .finally(() => {
        detailMBTilesLoading.hidden = true;
        detailMBTilesProgress.hidden = true;
        updateStorageReport();
      });
    })
  }

  document.getElementById('ceramic-mbtiles-download-button').onclick = async function(e) {
    ceramicMBTilesLoading.hidden = false;
    ceramicMBTilesProgress.hidden = false;
    ceramicMBTilesCached.hidden = true;

    // TODO: Remember to update the cache version if/when the MBTiles file is updated
    caches.open("ceramic-mbtiles-v1")
    .then((cache) => {
      fetch(CERAMIC_MBTILES_PATH)
      .then((response) => {
        // Clone the response for "measurement" use
        // https://jakearchibald.com/2014/reading-responses/
        const clonedResponse = response.clone();

        trackFetchProgress({ clonedResponse, statusElement: ceramicMBTilesProgress });

        if (!response.ok) {
          alert(`Failed to download offline ceramic map. (${response.status} ${response.statusText})`);
          console.error(response);
          return
        }
        return cache.put(CERAMIC_MBTILES_PATH, response);
      })
      .finally(() => {
        ceramicMBTilesLoading.hidden = true;
        ceramicMBTilesProgress.hidden = true;
        updateStorageReport();
      });
    })
  }

  document.getElementById('refresh-stats-button').onclick = async function(e) {
    updateStorageReport();
  }

  document.getElementById('hide-uncertain').onchange = async function(e) {
    document.querySelector(`link[href*=hide-unsure-markers]`).disabled = !e.target.checked
  }

  updateStorageReport();
}

window.onload = () => init();

window.onhashchange = () => {
  const activeLandmarkSlug = window.location.hash.split('#')[1];
  if (landmarkEntities.has(activeLandmarkSlug)) {
    const { landmark, group, marker, polyLine } = landmarkEntities.get(activeLandmarkSlug);
    group.addTo(map);

    if (landmark.location) {
      map.flyTo(landmark.location.value, 18);
      marker.openPopup();
    }
    if (landmark.track) {
      map.flyToBounds(polyLine.getBounds());
      polyLine.openPopup();
    }
  }
}
