# Landmarks

### Confusing cases:

Pla de Les Vinyes: Just south of the highway from Costa de Baix
El Vinyer: 1km NNE of El Moli d'en Robert
Can Vinyes: Just at the start of the road to La Vajol
La Solana d'en Vinyes: WSW of Coll de Perilló
Prat dels Vinyals: Only shown on ceramic tiles. Just west of the Pi de Can Seguer

Can Lleona is at the start of the road to La Vajol
Mas Lleona is at the southern extreme of the terme

C. de Lli is just NE of Castell de Cabrera
There's another C. de Lli just south of St. Andreu d'Oliveda

Pedrera del Perdigó: Just E of Mas Castell

Mas d'en Mas: 200m SE of Can Gallat
Solana d'en Mas: 200m NW of Mas Riu
Mas Riu: shown on zoom level 16 in the location of La Solana
Prats de mas riu: on zoom level 17, 18 around la Solana
El Riu: 200m east of el Quintà

Mas Balló: Up in the amfiteatre
Can Balló: Just SE of Tapis

La Cardona: On the other side of el vial
Can Cardona dels Vilars

Can Duran: Els Vilars
Mas Duran: In France, just beyond Puig de Pedrisses

Can Quera: 3km ENE of Castell de Cabrera
La Quera: Beyond Tapis off the highway, see "Castanyers de la Quera"

On ICGC we have "La Central", "La Central Municipal" (200m north of Pallera d'en Josepico).
On the ceramic tiles we have "Central d'en Cabot" and "central"
Further downriver we have on ICGC "Hotel La Central"
PR mentions "La Central d'en Grida", by the Font de l'Aranyó, same as "Hotel la Central"?

Can Pericot: Close to Tapis. See nonextant mas "Mas Oliva"
Mas Pericot: On the southern tip of the terme

Can Balló: 200m ESE of Tapis
Mas Balló: 300m SE of Puig de l'Evangeli

El Puig: 500m NE of St. Miquel de Fontfreda
El Puig: 1km W of Mas Balló

Can Roger: 400m ESE of Tapis
Mas Roger: 400m ENE of la Forestal

Can Muntada used to be known as "El Vilardell"
Vilardell: Nonextant mas somewhere on the road to St. Andreu. Fountain too.

La Muntada: 1km ESE of Corral de la Falguerona
Can Muntada: 1km NE of town
