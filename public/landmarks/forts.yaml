name: Castell de Falcó
alpha: falco
location:
  value: [42.37512, 2.66719]
  credit: ICGC
  unsure: true
description:
  - credit: Pere Roura
    value: |
      D’ aquest castell, avui dia no se’n veu cap rastre. Situat al Puig Falcó
      (1095 m), cim constituït per dos blocs rocosos bessons molt estimbats per
      la banda nord i est. El cim és aspre i només al vessant est hi prospera el
      faig; a la banda nord hi ha clapes de “boixa”, una espècie de neret. És un
      bon mirador de Fraussa, la vall de Maçanet i el golf de Roses. El nom fa
      referència a la presència de falcon, “ocell rapinyaire”.

      Aquest castell roquer era una fortificació de tradició antiga, que si calia
      servia de refugi. Formava part de la xarxa defensiva fronterera del comtat
      de Besalú i cobria la vall o demarcació de Maçanet, per la banda de ponent.
      És citat l’any 936 castello Falcone i el 1142, Roc de Falcó, sempre marcant
      els límits amb l’alou de Costoja. El cens de 1292, li assignava una
      guarnició de 12 homes. Suposem que al segle XIV, ja estava abandonat, però
      en els documents encara apareix el 1454, 1531 i en un del 1664, on es parla
      de la Roca de Falcó i el castell de Falcó i adverteix que eren molt
      propers, però no la mateixa cosa.
    url: https://www.massanetdecabrenys.com/castells.html
  - credit: Pere Roura
    url: ../els-noms-del-terme.html
    value: |
      Cim de 1.095 m constituït per dos blocs rocosos bessons, molt estimbats
      per la banda nord i est, que separa els termes de Maçanet i Albanyà. El
      cim és aspre, amb vegetació de “boixa”, una mena de neret, i només al
      vessant est prospera el faig. Damunt d’aquest pic s’havia bastit una
      fortificació de tradició antiga que, si calia, servia de refugi. Formava
      part de la xarxa defensiva fronterera del comtat de Besalú i cobria la
      vall o demarcació de Maçanet per la banda de ponent junt amb els castells
      d’Arget i Grillera. D’aquest castell avui no en queda cap rastre. El nom
      prové del falcó, “ocell de presa”.

    mentions:
      # El 936, llegim, *Custogia afrontat in parte orientis in castello
      # Falcone* (NH7-36).
      - year: 936
        mixed: |
          El 936, llegim, *Custogia afrontat in parte orientis in castello
          Falcone*
        citation: NH
        reference: 7-36
      # El 1142, *Roc de Falcó*, sempre marcant els límits de l’alou de Costoja.
      - year: 1142
        mixed: |
          El 1142, *Roc de Falcó*, sempre marcant els límits de l’alou de
          Costoja.
      # El 1168, *rocam de Falcone* (BPAlart 1168).
      - year: 1168
        quote: rocam de Falcone
        citation: BPAlart
        reference: 1168
      # L’any 1292, el castell de Falcó tenia una guarnició de 12 homes.
      - year: 1292
        note: el castell de Falcó tenia una guarnició de 12 homes
      # El 1318, *castell de Falcó* (BPAlart 1318).
      - year: 1318
        quote: castell de Falcó
        citation: BPAlart
        reference: 1318
      # El 1531, en els límits de la vegueria de Besalú, *del coll dels Horts
      # puja al castell de Falcó i d’aquí davalla al Riu Major y pas dels
      # traginers, camí real qui va de Costoja a Figueras*.
      - year: 1531
        mixed: |
          El 1531, en els límits de la vegueria de Besalú, *del coll dels Horts
          puja al castell de Falcó i d’aquí davalla al Riu Major y pas dels
          traginers, camí real qui va de Costoja a Figueras*
      # El 1589, *Havem venudas las aglans de la montanya de Falcó anant
      # Falgarona* (ELLABC).
      - year: 1589
        quote: Havem venudas las aglans de la montanya de Falcó anant Falgarona
        citation: ELLABC
      # El 1652, *castell y rocha de Falcó* (Not. O. Morera, Fi431).
      - year: 1652
        quote: castell y rocha de Falcó
        citation: Not. Onofre Morera
        reference: Fi431
      # El 1773, *castell de Falcó* (CMC).
      - year: 1773
        quote: castell de Falcó
        citation: CMC
      # El 1918, *cingles de Falcó* (PCG).
      - year: 1918
        quote: cingles de Falcó
        citation: PCG
see_also:
  - Puig Falcó
  - Castell de Grillera
---
name: Castell de Grillera
alpha: grillera
alternateNames:
  - Castell del Bac de Grillera
location:
  value: [42.360027, 2.703798]
  credit: Marc Vila
  date: 2024-03-27
description:
  - credit: Pere Roura
    value: |
      Aquest castell desaparegut s’aixecava al cim del mateix nom, es tracta
      del conegut tall que fa la serra del Fau (1014 m), consisteix en una
      penya que fa tres bonys; l’oriental és el més espadat, i té una paret de
      35 metres.

      En aquesta roca de llevant, no fa massa temps hi havia rastres d’una
      paret i el pagesos de la contrada, un segle endarrere asseguraven
      l’existència d’altres vestigis. L’any 954 es parla del Roc de Grillera,
      Rocha Graiaria. L’any 1531, surt en els límits de la vegueria de Besalú,
      com a castell de Grillera. Altres documents dels segles XVII i XVIII,
      esmenten el castell de Grillera com a fita que separava el terme dels
      Vilars amb el Comú de la Vila.

      Suposem que es tractava d’una torre de guaita, anterior al segle X, que
      tenia relació amb les fortificacions properes de Falcó i d’Arget. En
      aquest indret hi ha els característics “coixinets de senyora”, planta
      espinosa que es fa en llocs molt àrids i desolats. Des d’aquí es veu una
      extensa panoràmica de l'Empordà, la Garrotxa i el Pirineu (Coma negra,
      Costabona, Canigó).
    url: https://www.massanetdecabrenys.com/castells.html
  - credit: Pere Roura
    url: ../els-noms-del-terme.html
    value: |
      També anomenat castell del Bac de Grillera, es tracta del conegut tall
      que fa la serra del Fau (1.016 m). Consisteix en una penya que fa tres
      bonys; l’oriental és el més espadat i té una paret de 35 m. En aquesta
      roca de llevant els pagesos de la contrada, un segle enrere, asseguraven
      l’existència d’una paret i altres vestigis, cosa que reforça la
      possibilitat d’haver-hi existit una torre de guaita anterior al segle X,
      que hauria tingut relació amb les fortificacions properes de Falcó i
      Arget. El nom prové de “grallera”, lloc on nien o sovintegen les gralles
      o graules.
    mentions:
      # L’any 952 ens consta la *Rocha Graiaria* (ACB-28), i el 954, *podiola
      # qui dicitur Grillera* (NH 11-181).
      - year: 952
        mixed: L’any 952 ens consta la *Rocha Graiaria*
        citation: ACB
        reference: 28
      # i el 954, podiola qui dicitur Grillera
      - year: 954
        quote: podiola qui dicitur Grillera
        citation: NH
        reference: 11-181
      # El 1454, *castell de Grillera* (CMC).
      - year: 1454
        quote: castell de Grillera
        citation: CMC
      # El 1531, en els límits de la vegueria de Besalú, *afrontant dret al
      # castell de Grillera i tot aiguavés fins al coll dels Horts*.
      - year: 1531
        mixed: |
          El 1531, en els límits de la vegueria de Besalú, *afrontant dret al
          castell de Grillera i tot aiguavés fins al coll dels Horts*
      # El 1679, *lo castell de Grillera* (Not. F. de Gayolà, Fi476).
      - year: 1679
        quote: lo castell de Grillera
        citation: Not. Francesc de Gayolà
        reference: Fi476
photos:
  - Castell-de-Grillera--Marc-Vila--WhatsApp-Image-2024-03-27-at-11.02.01-AM.jpeg
see_also:
  - Roca Grillera
  - Castell de Falcó
---
name: Castell de Cornell
location:
  value: [42.39288, 2.67779]
  credit: ICGC
  unsure: true
description:
  credit: Pere Roura
  value: |
    El Cornell (976 m), és un cim prou conegut, es tracta d’un penyal de roca
    calcinal de forma allargassada, que culmina amb una aresta esquerdada, molt
    estreta. Les parets originen un espadat de 6O metres. El nom deriva de
    “cornella”, ocell de la família dels corbs.

    A l’Atles del Comtat de Besalú de Jordi Bolòs i Víctor Hurtado, hi figura
    el castell de Cornello els anys 948-949; també el Roc de Cornell, els anys
    954 i 1142. Era considerat un fita del Comú i per això el Roc i cingle de
    Cornell hi surten un reguitzell de vegades en el termenament d’aquestes
    terres. Deuria ser una fortificació, gairebé inaccessible de les mateixes
    característiques que les de Falcó i Grillera.
  url: https://www.massanetdecabrenys.com/castells.html
see_also:
  - El Cornell
---
# TODO: Think about how to unify this with the entry of the same landmark in
# the nonextant farmhouses collection.
name: Castell o Força de la Masó
location:
  value: [42.39137, 2.75500]
  credit: ICGC
  unsure: true
description:
  credit: Pere Roura
  value: |
    Era un mas fortificat, que estava situat a uns 200 m al sud-oest del mas
    Quintà, en un lloc conegut com el Pla de la Masó i al costat d’uns camps
    que en diuen Les Planes. Avui dia el bosc cobreix tot el recinte, del qual
    només se’n conserven els fonaments, algunes parets d’un parell de pams
    d’alçada, també trossos de teula i encara dóna la sensació que havia estat
    un gran edifici. Sembla que fou desmantellat totalment amb l’ampliació del
    mas Quintà al segle XIX.

    La primera referència escrita data de primers del segle XIV, (1318), que
    parla del “camí que va a la Masó”. Fou propietat dels Avinyó i després dels
    senyors de Darnius, que s'en titulaven barons de Darnius, de Les Illes i
    senyors del castell de La Masó. El 1550, Magdalena de Darnius, filla de
    Pere de Darnius, es casà amb Felip d’Ardena, des de llavors els Ardena es
    convertiren en senyors de La Masó fins al segle XVIII. Posseir un castell
    sempre era un senyal de prestigi i de domini; per això sempre feien constar
    a la capçalera dels capbreus que exercien els drets com a “senyors del
    castell o força de la Masó”.

    Cobraven delmes i censos d’una vintena de masos i terres, compartits amb el
    monestir de St. Pere de Camprodon, situats a la ribera d’Ardenya, Rinadal i
    Tapis i d’algunes cases del carrer de la Borriana.

    La recaptació anava a càrrec d’un procurador i les actes les feia sempre el
    notari del castell de Darnius. Durant els segles XVII i XVIII, molt sovint
    s’arrendava la senyoria a negociants i pagesos acabalats de Maçanet.

    Com hem dit, es tractava d’un mas fortificat, que tenia al voltant algunes
    terres que es mesuraven aleshores amb 12 jornades de llaurada de bous.
    Dintre aquesta tinença de terra hi havia un molí propi del castell, a la
    ribera d’Ardenya. Tenia cura del castell i terres, el pagès del Quintà, que
    a més feia les funcions de majordom i masover. Sembla que l’edifici servia
    de residència estiuenca dels senyors i altres visites esporàdiques. A
    darrers del segle XVIII, ja s’havia venut als Quintà i només s’utilitzava
    per a tancar-hi el bestiar. En aquesta època els Ardena alternaven el
    domicili de Perpinyà amb Barcelona.

    L’edifici a poc a poc s’anava ensorrant, el 1840, Pascual Madoz en el seu
    “diccionari”, el descriu com “ un gran casal enrunat”. Cal recalcar que
    aquest castell, esborrat de la memòria les maçanetencs, m’hauria estat molt
    difícil situar el seu emplaçament, sense l’ajuda de l’amic Joan Pagès Bosch
    a qui reconec la col·laboració.
  url: https://www.massanetdecabrenys.com/castells.html
see_also:
  - La Masó
---
name: Palau del Pi
location:
  value: [42.38422, 2.77202]
  unsure: true
description:
  credit: Pere Roura
  value: |
    El Pi és un mas desaparegut, situat al pla del mateix nom a la riba
    esquerra del Rinadal que pertanyia el 1352 a Guillem Pi. Al segle XVI,
    Antoni Vilanova, va reformar el mas i el convertí en un gran casal. Des de
    llavors s’anomenà el Palau del Pi fins almenys el 1664. Llindava amb can
    Llaona, el Rinadal, Can Sunyer i el mas Riu. Durant els primers anys del
    segle XVIII, la família Vilanova que havien sigut gent acabalada, s’anà
    venent totes les terres per fer vinya, llavors aquesta contrada es conegués
    com el vinyer del Pi.
  url: https://www.massanetdecabrenys.com/castells.html
see_also:
  - El Pi
---
name: Castell de Cabrera
location:
  value: [42.40744, 2.77313]
  credit: ICGC
description:
  credit: Pere Roura
  value: |
    Encimbellat damunt d’un penyal granític imponent, al nord-est del poble, a
    850 m d’alçada, es troben els vestigis del que fou el castell de Cabrera.
    Només es accessible per la banda nord; a migdia s’obre una timba quasi
    vertical de 65 m. El castell ocupava tot el cim i aquesta situació el
    convertia en una magnífica talaia. Des d’aquí podem veure una de les
    millors panoràmiques de la vall de Maçanet i La Vajol. Alhora aquesta mola
    rocosa, es pot veure gairebé d’arreu de l’Alt Empordà.

    Dessota el castell, a llevant hi ha l’anomenat coll de la Gàbia(785 m),que
    fa referència a l’existència en aquest lloc d’una gàbia o instrument de
    suplici, on s’exposaven els presoners a l’escarni públic i pertanyia al
    domini del castell. Per aquest indret hi passa el camí que prové de
    Maçanet, al nord s’enfila la carretera de La Vajol a Les Salines, que
    segueix l’antic camí de bast i ramader que carenejava la serra i duia al
    Vallespir. De la carretera, un petit corriol ens porta directament al
    castell. Més al nord per sobre la carretera hi ha el Pla de la Pastera (883
    m), nom que té a veure amb el “pasturatge” que s’hi havia fet antigament.
    Carretera amunt, trobem el Coll de la Biga (894 m) i el Coll de les Cordes
    (920 m), noms que tindrien a veure amb l’explotació forestal i les tragines
    respectivament.

    La penya és un estrep avançat de la serra de Les Salines, que s’estén cap
    el nord-oest, ambdues s’uneixen amb l’anomenat Coll del Castell. La
    vegetació dels voltants l’ocupa l’alzinar, de mida petita, perquè ha de
    lluitar contra el clima i el rocam. Engarrepats a la roca hi ha alguns
    exemplars d’alzina i càdec que podem considerar-los uns “bonsais
    centenaris”.

    Actualment les restes de la fortalesa s’aixequen al caire dels espadats,
    aquestes queden reduïdes a una cisterna, la base d’una torre quadrada i
    alguns murs de poca alçada. L’edifici ocupava una extensió de 289 metres
    quadrats amb un perímetre de 76 m.

    (Text complet disponible a massanetdecabrenys.com)
  url: https://www.massanetdecabrenys.com/castells.html
links:
  - text: Castell de Cabrera (Viquipèdia)
    url: https://ca.wikipedia.org/wiki/Castell_de_Cabrera_(Alt_Empord%C3%A0)
---
#name: Castell de Maçanet
# This one is quite involved. Will need to think about how to incorporate so
# much text and imagery. Perhaps an abridged version on the map pin popover
# and the option to go to the full description page.
# See: https://www.massanetdecabrenys.com/castells.html
#---
name: Fort de Maçanet
alternateNames:
  - Torre Carlina
location:
  value: [42.38493, 2.74944]
  credit: ICGC
links:
  - text: Torre Carlina (ubi.cat)
    url: https://ubi.cat/llistat/torre-carlina/
