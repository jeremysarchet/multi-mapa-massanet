title: Mines per tipus de mineral
description:
  credit: Pere Roura
  value: |
    L'explotació del subsòl ha estat des del segle XVIII, una pràctica comuna,
    de molts maçanetencs, La mineria es fornia de ma d'obra provinent,
    sobretot, de la pagesia, i malgrat que avui dia no quedi cap explotació
    oberta, encara hi ha molts homes que hi havien treballat.

    Cal remarcar la diferencia entre una mina i una pedrera. Una mina és una
    obra subterrània mitjançant galeries i pous que permeten l'explotació d'un
    filó d'un mineral; en canvi, una pedrera és una explotació a cel obert i
    s'accedeix al mineral fent desmunts en el terreny.

    Els minerals no estan distribuïts d'una manera geogràficament uniforme, per
    això abans de començar una explotació és necessari fer unes feines de
    recerca i caracterització del jaciment, que són de llarga durada, costoses
    i de resultats incerts. L'extracció del mineral en llocs allunyats i
    inaccessibles encareixen l'explotació i el transport.

    Sovint els jaciments es comencen a explotar pels indrets on la llei és més
    rica, prop de la superfície i on els treballs són més fàcils. A mesura que
    les explotacions avancen a major profunditat, els problemes augmenten,
    perquè cal remoure més materials per obtenir la mateixa quantitat de
    mineral o menys, cosa que sol ser molt corrent.

    Totes aquestes característiques i el preu de mercat, s'havien de tenir molt
    en compte per la rendibilitat d'una explotació. És ben cert que l’èxit de
    les extraccions, estaven condicionades per l'atzar; per això a Maçanet al
    llarg de la història, bona part dels projectes miners, serien abandonats
    tot just començades les feines.

    També hem pogut comprovar que molts jaciments abandonats, anys més tard
    serien reactivats per al tres empreses a la recerca de la fortuna.

    Per iniciar una explotació minera, era necessari, que l'administració
    concedís el permís d'obertura. En l'expedient hi figurava, la superfície
    reservada per l'explotació del jaciment; expressada en pertinences.

    Una pertinença, equival a una Hectàrea (10.000 m2) Això es feia per
    garantir que cap més empresa pogués intervenir en el jaciment registrat.

    A l'Arxiu Històric de Girona, hi ha dipositat un fons de la Delegació
    d’Indústria, amb el títol de "Concessions Mineres", que conté els
    expedients d'obertura i ampliació de les explotacions mineres de la
    província de Girona, des de l'any 1864 al 1980.

    A l'Arxiu Històric de Figueres, hi trobem un fons del Casino Menestral
    Figuerenc, que recull un inventari de les mines de l'Alt Empordà de l'any
    1888.

    Tots aquests documents han estat fonamentals per fer aquest estudi, que
    pretén recollir les prospeccions i explotacions mineres més importants del
    nostre terme, durant els tres darrers segles.

    El següent inventari es distribuirà per tipus de minerals; és a dir, cada
    mineral portarà la relació dels seus jaciments. També s’inclourà, el nom de
    l'explotació, l'any, el nom de l'explotador o de l'empresa i altres dades
    complementaries.
  url: https://www.massanetdecabrenys.com/mines.html
groups:
  - name: El sabonet
    description:
      credit: Pere Roura
      value: |
        És el mineral que més quantitat se n'ha extret a Maçanet. Els filons de
        més qualitat es trobaven en dos sectors: el primer transcorria pel Puig
        de la Calma, Gurugú i Coma de Nivà, i l'altre del peu de Fraussa fins a
        Les Salines.

        L’abundància d'aquest mineral mantindria gairebé la professió de
        minaire durant 100 anys, amb una dedicació estable d'uns 30 homes des
        del 1905.
      url: https://www.massanetdecabrenys.com/mines.html
    members:
      - Pedrera del Gravat
      - Mina d'en Sunyer
      - Mina La Llosera
      - Mina La Ampordanesa
      - Mina Santa Maria
      - Mina Magdalena
      - Mina Pepita
      - Mina Blanca
      - Mina Rafaela
      - Mina d'en Servitja
      - Mina La Pobre
      - Mina Tercera
      - Mina La Taupa
      - Mina La Olvidada
      - Mina Perxés de Niubó
      - Mina d'en Saguer
      - Mina Trinidad
      - Mina Próspera
      - Mina Pirenaica
      - Mina Cusi
      - Mina Grau
      - Mina Mossén Roura
      - Mina Victoria
  - name: El ferro
    description:
      credit: Pere Roura
      value: |
        A Maçanet abunden els meners de ferro, però són filons petits i de mineral
        de poca llei. Es tracta d’òxids de ferro: siderita, magnetita, limonita i
        pirita magnètica. Els jaciments que han estat explotats es troben a la cara
        sud de la serra de Montdevà i al vessant de la muntanya, que va des del
        Puig Falcó fins a Fraussa.

        Durant la primera meitat del segle XlX, quan les dues fargues estaven al
        seu apogeu, s'intensificaren les prospeccions, però el mineral obtingut,
        només cobria el 20 per cent de les necessitats.
      url: https://www.massanetdecabrenys.com/mines.html
    members:
      - Serrat dels Meners
      - Mina Mariana
      - Mina Colorada
      - Mina Fuster
      - Mina Bella Conchita
      - Minas Las Tres Hermanas
      - Mina Teresita
      - Mina Buena Presa
      - Mina Agustina
      - Mina Teresa
      - Santa Bárbara
      - Mina Unión
      - Mina Democràcia
      - Mina Edisson
      - Mina Lealtad
      - Mina Emilia
      - Santa Teresita
      - Mina Maria
  - name: El coure
    description:
      credit: Pere Roura
      value: |
        L'explotació del coure va ser molt important al segle XlX; estava
        concentrat al vessant sud de Montdevà i a la seva part més oriental. La
        major part de les boques de mina es trobaven a la Solana del Rimal, just al
        límit dels termes de St. Llorenç i Maçanet,

        Per això estaven registrades en ambdós termes, perquè els filons
        s'endinsaven a la serra de Montdevà que es terme de Maçanet.
      url: https://www.massanetdecabrenys.com/mines.html
    members:
      - Mina Emilia II
      - Mina Juana
      - Estrella de Maria
      - Mina La Carbonera
      - Mina L'Espingarda
      - Mina Severiano
      - Mina Consuelo
      - Mina Santa Teresita
  - name: El marbre
    description:
      credit: Pere Roura
      value: |
        El mineral estava concentrat en tres sectors: Les Salines, El Balló i El
        Grau. L'extracció del mineral va destacar més per la qualitat que no per la
        quantitat. Sobretot el de Les Salines i el del Bac de Llinars.
      url: https://www.massanetdecabrenys.com/mines.html
    members:
      - Pedrera de Les Salines
      - Pedrera del Balló
      - Pedrera del Bac de Llinars
      - Pedrera del Grau
      - Pedrera de Coma de Nivá
  - name: La plata
    description:
      credit: Pere Roura
      value: |
        Totes les prospeccions de mineral de plata, estaven centrades al Clot de Les
        Salines. L’èxit d'aquestes proves sempre fou amanit amb molta fantasia. Que
        sapiguem, només d’una situada a mig camí entre la Collada dels Pous i la
        capella, se n'extragué una petita quantitat.
      url: https://www.massanetdecabrenys.com/mines.html
    members:
      - Mines de Les Salines
  - name: L'or
    description:
      credit: Pere Roura
      value: |
        L'any 1940, Antoni Amat Rotllan, va demanar l'obertura d'una mina d'or, a
        Montdevà, situada al Clot de l'Infern, sota el cingle de Cresta de Gall.

        L'or, segons es deia, estava en una penya barrejat amb un quars molt dur.
        Desconeixem els resultats, però si sabem que el 1941 ja no s'explotava, per
        ordre del governador civil, perquè era "un mineral inexplotable por
        particulares".
      url: https://www.massanetdecabrenys.com/mines.html
  - name: El carbó
    description:
      credit: Pere Roura
      value: |
        Dintre el terme al segle XIX, es van descobrir alguns filons molt prims de
        lignit, que no van ser explotats per manca de rendibilitat.
      url: https://www.massanetdecabrenys.com/mines.html
    members:
      - Mina Fortiana
      - Mina Igualdad
      - Mina Albertí
      - Mina Conde Alfonso
      - Mina Roberto
  - name: El Níquel
    description:
      credit: Pere Roura
      value: |
        En el lloc conegut com el Bosc d'en Costa, al sud-est de la capella
        d'Oliveda, a uns 50 m del Clot del Mener, hi ha les restes d'un pou on
        s'havia extret mineral de níquel els anys 1868-1869. La mina s'anomenava
        Buena Suerte, i va ser abandonada a causa de la poca llei del mineral. Es
        va intentar, sense èxit, una nova explotació 1869-1886.

        El filó, ja esgotat, no contenia gens de níquel; però la seva composició
        mineral era molt notable des del punt de vista geològic: moscovita, pirita
        arsenical, clorita, amfíbol, pirita de coure i plata.

        El que interessava més era la poca consistència de la roca; que s'engrunava
        fàcilment i això afavoria la separació dels minerals.
      url: https://www.massanetdecabrenys.com/mines.html
  - name: La pedra tosca
    description:
      credit: Pere Roura
      value: |
        La pedra tosca, s'emprava per a la construcció. La seva lleugeresa, feia
        que s'utilitzés per fer voltes i tempanells; i també com a element
        decoratiu en carreus d'obertures, arcs i façanes,

        A Maçanet hi havia dues pedreres: la més coneguda era sota del mas Pericot,
        a tocar el Rimal; i l'altra al Clot de la Tosca, prop de la Rellera del Fau.

        Al segle XIX, quan feien obres, parlaven de "anar a fer tosca cap al terra
        fort".
      url: https://www.massanetdecabrenys.com/mines.html
