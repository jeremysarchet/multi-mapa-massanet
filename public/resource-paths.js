
const TOPO_14_18_MBTILES_PATH = './topo.mbtiles';
const CERAMIC_MBTILES_PATH = './ceramic-tms-webp.mbtiles';

const TOPO_12_13_TILESET_URLS = [
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2076/1513.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2076/1514.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2076/1515.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2076/1516.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2077/1513.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2077/1514.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2077/1515.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2077/1516.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2078/1513.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2078/1514.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2078/1515.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2078/1516.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2079/1513.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2079/1514.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2079/1515.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2079/1516.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2080/1513.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2080/1514.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2080/1515.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2080/1516.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2081/1513.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2081/1514.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2081/1515.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/12/2081/1516.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4153/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4153/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4153/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4153/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4153/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4153/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4153/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4154/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4154/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4154/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4154/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4154/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4154/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4154/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4155/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4155/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4155/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4155/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4155/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4155/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4155/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4156/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4156/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4156/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4156/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4156/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4156/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4156/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4157/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4157/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4157/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4157/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4157/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4157/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4157/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4158/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4158/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4158/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4158/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4158/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4158/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4158/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4159/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4159/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4159/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4159/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4159/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4159/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4159/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4160/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4160/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4160/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4160/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4160/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4160/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4160/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4161/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4161/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4161/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4161/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4161/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4161/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4161/3032.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4162/3026.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4162/3027.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4162/3028.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4162/3029.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4162/3030.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4162/3031.png",
  "https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/13/4162/3032.png",
];
