importScripts('lastUpdate.js');

console.log('service-worker.js: hostname', location.hostname);

const CORE_RESOURCES_CACHE_NAME = `main-${LAST_UPDATE}`;

const CORE_RESOURCE_PATHS = [
  "/",
  "/index.html",
  "/index.js",
  "/style.css",
  "/favicon.ico",
  "/resource-paths.js",
  "/hide-unsure-markers.css",
  "/utils.mjs",

  "/landmarks/fountains.yaml",
  "/landmarks/farmhouses.yaml",
  "/landmarks/nonextant-farmhouses.yaml",
  "/landmarks/trees.yaml",
  "/landmarks/bridges.yaml",
  "/landmarks/houses.yaml",
  "/landmarks/saddles.yaml",
  "/landmarks/pools.yaml",
  "/landmarks/mines.yaml",
  "/landmarks/monuments.yaml",
  "/landmarks/peaks.yaml",
  "/landmarks/forts.yaml",
  "/landmarks/mills.yaml",
  "/landmarks/chapels.yaml",
  "/landmarks/caves.yaml",

  "/manifest.json",

  // TODO: Look into why the font possibly isn't loading when offline
  "/fonts/BunaeroFree/font.woff",
  "/fonts/BunaeroFree/font.woff2",

  // Use e.g. `find scripts -type f | sed 's/^/\ \ "\.\//g' | sed 's/$/",/g' | sed '/DS_Store/d' | sort`
  "/scripts/Leaflet.TileLayer.MBTiles.js",
  "/scripts/js-yaml.min.js",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.css",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.css.map",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.mapbox.css",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.mapbox.css.map",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.mapbox.min.css",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.mapbox.min.css.map",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.min.css",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.min.css.map",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.min.js",
  "/scripts/leaflet-locatecontrol/dist/L.Control.Locate.min.js.map",
  "/scripts/leaflet/images/layers-2x.png",
  "/scripts/leaflet/images/layers.png",
  "/scripts/leaflet/images/marker-icon-2x.png",
  "/scripts/leaflet/images/marker-icon.png",
  "/scripts/leaflet/images/marker-shadow.png",
  "/scripts/leaflet/leaflet.css",
  "/scripts/leaflet/leaflet.js",
  "/scripts/leaflet/leaflet.js.map",
  "/scripts/sqljs-wasm/sql-wasm.js",
  "/scripts/sqljs-wasm/sql-wasm.wasm",

  "/images/coat-of-arms-maskable-icon-512.webp",
  "/images/coat-of-arms-maskable-icon-192.webp",
  "/images/2023-04-11-multi-mapa-massanet-screenshot.jpg",

  "/images/mapa-pere-roura-1995.jpg",
];

let paths = CORE_RESOURCE_PATHS;

if (location.hostname !== 'localhost') {
  paths = CORE_RESOURCE_PATHS.map((path) => '/multi-mapa-massanet' + path);
}

// https://stackoverflow.com/a/74498381
function addPathsIndividually(cache) {
  const promises = [];
  paths.forEach(path => {
    promises.push(
      cache.match(path)
      .then(response => {
        if (!response) {
          console.log('adding to cache:', path);
          // If the path is not cached, add it to the cache
          return cache.add(path)
            .catch(error => {
              console.error(`Unable to add ${path} to cache`, error);
            });
        }
        console.log('already in cache:', path);
      })
      .catch(error => {
        console.error(`Error checking cache for ${path}`, error);
      })
    )
  });
  return Promise.all(promises);
};

self.addEventListener('install', event => {
  console.log('adding paths', paths);
  event.waitUntil(
    caches.open(CORE_RESOURCES_CACHE_NAME)
      .then(cache => {
        return addPathsIndividually(cache) // Instead of `cache.addAll(paths)`
          .then(() => {
            // Clean up any old caches
            return caches.keys()
              .then(cacheNames => {
                return Promise.all(
                  cacheNames.filter(cacheName =>
                    cacheName !== CORE_RESOURCES_CACHE_NAME
                    && !cacheName.includes('mbtiles')
                  )
                  .map(cacheName => caches.delete(cacheName))
                );
              });
          })
      })
  );
});

async function cacheOnly(request) {
  const responseFromCache = await caches.match(request);
  if (responseFromCache) {
    return responseFromCache;
  }

  return new Response("Network error happened", {
    status: 408,
    headers: { "Content-Type": "text/plain" },
  });
}

async function cacheFirst(request) {
  const responseFromCache = await caches.match(request);
  if (responseFromCache) {
    return responseFromCache;
  }

  try {
    const responseFromNetwork = await fetch(request);
    return responseFromNetwork;
  } catch (error) {
    return new Response("Network error happened", {
      status: 408,
      headers: { "Content-Type": "text/plain" },
    });
  }
}

self.addEventListener('fetch', event => {
  if (event.request.url.endsWith('mbtiles')) {
    event.respondWith(cacheOnly(event.request));
  } else {
    event.respondWith(cacheFirst(event.request));
  }
});
