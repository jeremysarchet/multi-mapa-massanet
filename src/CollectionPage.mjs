import { readFileSync, writeFileSync } from 'fs';
import { load } from './scripts/js-yaml.mjs';
import { meta } from './reusable.mjs';

import { parseMarkdown } from './build.mjs';
import { createSlug, getTextForURL } from './utils.mjs';

export function CollectionPage(title, dataFile) {
  const page = load(readFileSync(dataFile));

  let content = '';

  for (const group of page.groups) {
    const { name, description, members } = group;
    content += `<div class="section">
      <a class="back-to-top" href='#'>A dalt</a>
      <h2 id='${createSlug(name)}'>
        ${name}
      </h2>
      ${description ?
        `<div class="description">
          ${parseMarkdown(description.value)}
          <table>
            ${description.credit ? `
              <tr>
                <th>Per</th>
                <td>
                  ${description.credit}
                </td>
              </tr>
            ` : ''}
            ${description.url ?
              `<tr>
                <th>Font</th>
                <td>
                  <a target="_blank" href="${description.url}">
                    ${getTextForURL(description.url)}
                  </a>
                </td>
              </tr>`
            : ''}
          </table>
        </div>` :
        ''
      }
      <ul>
        ${members ?
          `${members.map((memberName) => {
            return `<li>
              <a href="../marcadors/${createSlug(memberName)}.html">${memberName}</a>
            </li>`;
          }).join('\n')}` :
          '<em>Grup sense cap membre.</em>'
         }
      </ul>
    </div>`
  }


  const result = `<!DOCTYPE html>
<html lang="ca">
  <head>
    ${meta(
      `${title} | Multi mapa Maçanet`,
      'Marcadors per grup',
      `https://jeremysarchet.gitlab.io/multi-mapa-massanet/conjunts/${createSlug(title)}.html`,
    )}

    <link rel="stylesheet" type="text/css" href="../marcadors/landmark-details.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
  </head>

  <body>
    <a id="nav-back" href="../resum">Inici</a>
    <h1>${title}</h1>
    <div class="description">
      <p>
        ${parseMarkdown(page.description.value)}
      </p>
      <table>
        ${page.description.credit ? `
          <tr>
            <th>Per</th>
            <td>
              ${page.description.credit}
            </td>
          </tr>
        ` : ''}
        ${page.description.url ?
          `<tr>
            <th>Font</th>
            <td>
              <a target="_blank" href="${page.description.url}">
                ${getTextForURL(page.description.url)}
              </a>
            </td>
          </tr>`
        : ''}
      </table>
    </div>
    ${content}
  </body>
</html>
`;

  const outputFilePath = `public/conjunts/${createSlug(title)}.html`;
  try {
    writeFileSync(outputFilePath, result);
  } catch (error) {
    console.error(`Problem writing to file ${outputFilePath}`, error);
  }
}
