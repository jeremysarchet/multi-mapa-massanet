import { meta } from './reusable.mjs';

import { createSlug, getTextForURL } from './utils.mjs';

export function GroupPage(
  group,
  slug,
) {
  const { name, description, members } = group;

  return `<!DOCTYPE html>
<html lang="ca">
  <head>
    ${meta(
      `${name} | Multi mapa Maçanet`,
      'Marcadors per grup',
      `https://jeremysarchet.gitlab.io/multi-mapa-massanet/grups/${slug}`,
    )}

    <link rel="stylesheet" type="text/css" href="/marcadors/landmark-details.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
  </head>

  <body>
    <a id="nav-back" href="/resum">Tornar</a>
    <h1>${name}</h1>
    <div class="description">
      <p>
        ${description.value}
      </p>
      <table>
        ${description.credit ? `
          <tr>
            <th>Per</th>
            <td>
              ${description.credit}
            </td>
          </tr>
        ` : ''}
        ${description.url ?
          `<tr>
            <th>Font</th>
            <td>
              <a target="_blank" href="${description.url}">
                ${getTextForURL(description.url)}
              </a>
            </td>
          </tr>`
        : ''}
      </table>
    </div>
    <hr />
    <p><b>Membres del grup:</b></p>
    <ul>
      ${members.map((memberName) => {
        return `<li>
          <a href="../marcadors/${createSlug(memberName)}.html">${memberName}</a>
        </li>`;
      }).join('\n')}
    </ul>
  </body>
</html>
`;
}
