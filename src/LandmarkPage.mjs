import path from 'path';
import { meta } from './reusable.mjs';

import { parseMarkdown } from './build.mjs';

import {
  BUCKET_BASE_URL,
  DATE_OPTIONS,
  createSlug,
  getTextForURL,
} from './utils.mjs';

export function LandmarkPage(
  landmark,
  slug,
  categorySlug,
  categoryDisplayName,
  statusEmoji,
  statusText,
  allAttachmentMetadata,
  allPhotoMetadata,
  allCitations,
) {
  const title = `${landmark.name} | Multi mapa Maçanet`;
  const description = `Categoria: ${categoryDisplayName}`;
  const socialPageURL = `https://jeremysarchet.gitlab.io/multi-mapa-massanet/marcadors/${slug}`;
  let socialPhotoURL = '';
  if (landmark.photos) {
    const photoKey = landmark.photos[0];
    if (photoKey) {
      socialPhotoURL = `${BUCKET_BASE_URL}/600/${photoKey}.600.jpg`;
    }
  }

  // The `description` field in the YAML started out as a single object. Then
  // came landmarks having multiple descriptions. At some point, perhaps the
  // prudent thing to do is to migrate all the YAML such that `description`
  // is renamed to `descriptions` and all of them are YAML arrays, even those
  // which would be arrays of one. As a workaround, here's some logic to
  // gracefully handle both single descriptions and arrays of descriptions.
  let descriptions = ``;
  let descriptionObjects = [];

  if (landmark.description) {
    if (Array.isArray(landmark.description)) {
      descriptionObjects = landmark.description;
    } else {
      descriptionObjects = [landmark.description];
    }
  } else {
    descriptions = `
      <em>Sense cap descripció</em>
    `;
  }

  for (const description of descriptionObjects) {
    const attachmentMetadata = allAttachmentMetadata[description.attachment];
    descriptions += `
    <div class="description">
      ${description.title ? `<p><b>${description.title}</b></p>` : ''}
      ${parseMarkdown(description.value)}
      ${description.mentions ?
          `<ul class="mentions">
            ${description.mentions.map((mention) => {
              const { year, note, quote, mixed, citation, reference } = mention;
              function formattedCitationWithReference() {
                if (!citation) {
                  return '(sense citació)';
                }
                let result = citation;
                if (reference) {
                  if (citation.startsWith('Not.')) {
                    result += ', ' + reference;
                  } else {
                    result += ' ' + reference;
                  }
                }
                return `(${result})`;
              }
              function formattedMention() {
                if (quote) {
                  return `El ${year},
                    <q>${quote}</q>
                    ${formattedCitationWithReference()}.`;
                }
                if (note) {
                  return `El ${year},
                    ${note} ${formattedCitationWithReference()}.`;
                }
                if (mixed) {
                  return parseMarkdown(`${mixed} ${formattedCitationWithReference()}.`);
                }
              }
              return `<li>
                ${formattedMention()}
              </li>`;
            }).join('\n')}
          </ul>
          <div class="footnotes">
            ${(() => {
              const citations = description.mentions.map(
                (mention) => mention.citation
              ).filter((citation) => citation !== undefined);
              const citationsSet = new Set(citations)

              return Array.from(citationsSet).map((citation) => {
                const citationValue = allCitations[citation] ?? `${citation}: Citació no trobada.`;
                if (!(citation in allCitations)) console.error('Citation not found for', citation);
                return `<div>${parseMarkdown(citationValue)}</div>`;
              }).join('\n');
            })()}
          </div>`
      : ''}
      <table class="properties">
        ${description.credit ? `
          <tr>
            <th>Per</th>
            <td>
              ${description.credit}${
                description.date ?
                `, ${description.date.toLocaleDateString('ca-ES', DATE_OPTIONS)}`
              : ''}
            </td>
          </tr>
        ` : ''}
        ${description.url ?
          `<tr>
            <th>Font</th>
            <td>
              <a target="_blank" href="${description.url}">
                ${description.url_text ? description.url_text : getTextForURL(description.url)}
              </a>
            </td>
          </tr>`
        : ''}
        ${(description.attachment && attachmentMetadata) ? `
          <tr>
            <th>Enllaç</th>
            <td>
              <a target="_blank" href="../fitxers/${description.attachment}">
                ${attachmentMetadata.title}
              </a>
            </td>
          </tr>
          ${attachmentMetadata.type ? `<tr>
            <th>Tipus</th>
            <td>${attachmentMetadata.type ?? '-'}</td>
          </tr>` : ''}
          ${attachmentMetadata.credit ? `<tr>
            <th>Crèdit</th>
            <td>${attachmentMetadata.credit ?? '-'}</td>
          </tr>` : ''}
          ${attachmentMetadata.where_found ? `<tr>
            <th>Pàgina original</th>
            <td>
              <a target="_blank" href="${attachmentMetadata.where_found.url}">
                ${attachmentMetadata.where_found.text}
              </a>
            </td>
          </tr>` : ''}
          ${attachmentMetadata.original ? `<tr>
            <th>Fitxer original</th>
            <td>
              <a target="_blank" href="${attachmentMetadata.original}">
                ${description.attachment}
              </a>
            </td>
          </tr>` : ''}
          <tr>
            <th>Consultat</th>
            <td>
              ${attachmentMetadata.retrieved_on ?
                `${attachmentMetadata.retrieved_on.toLocaleDateString('ca-ES', DATE_OPTIONS)}
                `
              : '-'}
            </td>
          </tr>
        ` : ''}
      </table>
    </div>
    `;
  }

  let photos = '';
  for (const photoKey of landmark.photos ?? []) {
    const metadata = allPhotoMetadata[photoKey];
    const rawDate = metadata?.datetime_original ?? metadata?.date_created;

    const date = new Date(rawDate);
    let originalFileFromURL;
    if (metadata?.web_statement) {
      originalFileFromURL = decodeURIComponent(
        path.basename(new URL(metadata.web_statement).pathname)
      );
    }
    // Treating a date with all zeroes for the time value as having no time
    // value, in which case we present only the date. If, on the miraculous
    // chance that someone actually snapped a photo at midnight at zero
    // milliseconds, then this fact will not be reflected on the page.
    const timeIsZeroes = date.getUTCHours() === 0 &&
      date.getUTCMinutes() === 0 &&
      date.getUTCSeconds() === 0 &&
      date.getUTCMilliseconds() === 0;

    photos += `<div class="photo-with-metadata">
      <img src="${BUCKET_BASE_URL}/600/${photoKey}.600.jpg" />
      <div class="photo-metadata">
        ${metadata?.title ?
            `<h3>${metadata.title}</h3>` : ''}
        <table class="properties">
          <tr>
            <th>Peu de foto</th>
            <td>${metadata?.caption ?? '-'}</td>
          </tr>
          <tr>
            <th>Autor</th>
            <td>${metadata?.author ?? '-'}</td>
          </tr>
          ${metadata?.credit ? `<tr>
            <th>Crèdit</th>
            <td>${metadata?.credit ?? '-'}</td>
          </tr>` : ''}

          ${metadata?.page_name ? `<tr>
            <th>Font</th>
            <td>
              <a target="_blank" href="${metadata.page_name}">
                ${getTextForURL(metadata.page_name)}
              </a>
            </td>
          </tr>` : ''}

          ${metadata?.web_statement ? `<tr>
            <th>Original</th>
            <td>
              <a target="_blank" href="${metadata.web_statement}">
                ${originalFileFromURL}
              </a>
            </td>
          </tr>` : ''}

          <tr>
            <th>Data</th>
            <td>
              ${rawDate ?
                `${date.toLocaleDateString('ca-ES', DATE_OPTIONS)}${
                !timeIsZeroes ?
                `, ${date.toLocaleTimeString('ca-ES')}`
                : ''}`
              : '-'}
            </td>
          </tr>
          <tr>
            <th>GPS</th>
            <td>
              ${metadata?.gps_position ?
               `${metadata.gps_position.join(', ')}${metadata?.gps_altitude ?
                 `, ${metadata.gps_altitude}m`
                 : ''}`
               : '-'}
            </td>
          </tr>
          ${metadata?.make || metadata?.model ?
            `<tr>
              <th>Càmera</th>
              <td>${metadata.make} ${metadata.model}</td>
            </tr>`
          : ''}
          <tr>
            <th>Enllaç</th>
            <td><a target="_blank" href="${BUCKET_BASE_URL}/${photoKey}">${path.basename(photoKey)}</a></td>
          </tr>
        </table>
      </div>
    </div>
    `;
  }

  return `<!DOCTYPE html>
  <html>
  <head>
    ${meta(title, description, socialPageURL, socialPhotoURL)}

    <link rel="stylesheet" type="text/css" href="landmark-details.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">

    ${(landmark.location || landmark.track) ?
      `<link rel="stylesheet" href="../scripts/leaflet/leaflet.css" />
      <script src="../scripts/leaflet/leaflet.js"></script>
      ${landmark.track ? `<script src="../scripts/gpx.min.js"></script>` : ''}
      <script>
          function init() {
              const map = L.map('map', {
                  center: [42.386, 2.749],
                  zoom: 18,
              });
              const topoMonICGCCache = L.tileLayer('https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/{z}/{x}/{y}.png', {
                  maxZoom: 19,
                  attribution: 'ICGC'
              }).addTo(map);

              const ExtendedIcon = L.Icon.Default.extend({ options: {}});

              ${landmark.location ?
                `${landmark.location.unsure
                ? `const circle = L.circle([${landmark.location.value}], {
                    color: 'gold',
                    fillColor: 'gold',
                    fillOpacity: 0.25,
                    radius: 80,
                  }).addTo(map);`
                : `var marker = L.marker([${landmark.location.value}]).addTo(map);`}
                map.setView([${landmark.location.value}], 18);`
                : ''
              }
              ${landmark.track ?
                `const trail = new L.GPX(
                  '../tracks/${landmark.track.file}',
                  {
                    async: true,
                    marker_options: {
                      startIcon: new ExtendedIcon({ className: 'track-start-icon' }),
                      endIcon: new ExtendedIcon({ className: 'track-end-icon' }),
                      shadowUrl: 'scripts/leaflet/images/marker-shadow.png',
                      iconSize: [25, 41],
                      iconAnchor: [12, 41],
                      shadowSize: [41, 41],
                      shadowAnchor: [12, 41],
                    },
                    polyline_options: {
                      color: 'coral',
                      weight: '5'
                    }
                  }
                );
                trail.on('loaded', function(e) {
                  const gpx = e.target;
                  const trackStats = {
                    trackName: gpx.get_name(),
                    distanceMeters: gpx.get_distance(),
                    distanceKilometers: gpx.get_distance() / 1000,
                    elevationGain: gpx.get_elevation_gain().toFixed(0),
                    elevationLoss: gpx.get_elevation_loss().toFixed(0),
                    elevationMin: gpx.get_elevation_min().toFixed(0),
                    elevationMax: gpx.get_elevation_max().toFixed(0),
                  }
                  document.getElementById('distance').innerText = trackStats.distanceKilometers.toFixed(1) + ' km';
                  document.getElementById('elevation-gain').innerText = trackStats.elevationGain + ' m';
                  document.getElementById('elevation-loss').innerText = trackStats.elevationLoss + ' m';
                  document.getElementById('elevation-min').innerText = trackStats.elevationMin + ' m';
                  document.getElementById('elevation-max').innerText = trackStats.elevationMax + ' m';
                  const polyLine = gpx.getLayers()[0];
                  map.fitBounds(polyLine.getBounds());
                });
                trail.addTo(map);
                `
                : ''
              }

              map.addEventListener('contextmenu', (event) => {
                const locationString = '[' + event.latlng.lat.toFixed(5) + ', ' +  event.latlng.lng.toFixed(5) + ']';
                alert("Coordenades: " + locationString);
              });

          }
      </script>`
      : ''
    }
  </head>

  <body ${(landmark.location || landmark.track) ? 'onLoad="init()"' : ''}>
    ${(landmark.location || landmark.track) ?
      `<a id="nav-back" href="../#${slug}">Veure-ho al mapa</a>`
      : `<a id="nav-back" href="../">Tornar al mapa</a>`}
    <h1>${landmark.name}</h1>
    <table class="properties">
      <tr>
        <th>Categoria</th>
        <td><a href="../resum#${categorySlug}">${categoryDisplayName}</a></td>
      </tr>
      ${landmark.alternateNames ?
        `<tr><th>Altres noms</th><td>${landmark.alternateNames.join(', ')}</td></tr>`
      : ''}
    </table>
    <hr />
    ${landmark.groups ? `
      <p><b>Grups:</b></p>
      <ul>
        ${landmark.groups.map(({ page, section }) => {
          return `<li>
            <a href="../conjunts/${createSlug(page)}.html#${createSlug(section)}">${section}</a>
          </li>`;
        }).join('\n')}
      </ul>
      <hr />
    ` : ''}
    <table class="properties">
      <tr>
        <th>Geolocalització</th>
        <td>${statusEmoji} ${statusText}</td>
      </tr>
      ${landmark.location?.value ?
        `<tr>
          <th>Coordinades</th>
          <td>${landmark.location?.value.join(', ') ?? '-'}</td>
        </tr>`
        : ''
      }
      ${landmark.location?.elevation ?
        `<tr><th>Alçada</th><td>${landmark.location.elevation}</td></tr>`
      : ''}
      ${landmark.location?.credit ? `
        <tr>
          <th>Per</th>
          <td>
            ${landmark.location.credit === 'MM' ?
              `<a href="../images/mapa-pere-roura-1995.jpg" target="_blank">
                Mapa Maçanet i els seus límits</a>` :
              `${landmark.location.credit}${
                landmark.location?.date ?
                `, ${landmark.location.date.toLocaleDateString('ca-ES', DATE_OPTIONS)}`
              : ''}`
            }
          </td>
        </tr>
      ` : ''}
      ${landmark.track?.credit ? `
        <tr>
          <th>Per</th>
          <td>
            ${landmark.track.credit}${landmark.track?.date ?
              `, ${landmark.track.date.toLocaleDateString('ca-ES', DATE_OPTIONS)}`
            : ''}
          </td>
        </tr>
      ` : ''}
      ${landmark.track?.file ?
        `
        <tr>
          <th>Distància</th>
          <td id="distance"></td>
        </tr>
        <tr>
          <th>Desnivell positiu</th>
          <td id="elevation-gain"></td>
        </tr>
        <tr>
          <th>Desnivell negatiu</th>
          <td id="elevation-loss"></td>
        </tr>
        <tr>
          <th>Alçada màxima</th>
          <td id="elevation-max"></td>
        </tr>
        <tr>
          <th>Alçada mínima</th>
          <td id="elevation-min"></td>
        </tr>
        <tr>
          <th>Fitxer track</th>
          <td><a href="../tracks/${landmark.track.file}">${landmark.track.file}</a></td>
        </tr>
        `
        : ''
      }
    </table>
    ${(landmark.location || landmark.track) ? '<div id="map"></div>' : ''}
    <hr />
    ${descriptions}
    <hr />
    ${landmark.see_also ? `
      <p><b>Vegeu també:</b></p>
      <ul>
        ${landmark.see_also.map((landmarkName) => {
          return `<li>
            <a href="${createSlug(landmarkName)}.html">${landmarkName}</a>
          </li>`;
        }).join('\n')}
      </ul>
      <hr />
    ` : ''}
    ${landmark.links ? `
      <p><b>Enllaços:</b></p>
      <ul>
        ${landmark.links.map((link) => {
          return `<li>
            <a target="_blank" href="${link.url}">${link.text}</a>
          </li>`;
        }).join('\n')}
      </ul>
      <hr />
    ` : ''}
    ${photos}
  </body>
  </html>
  `;
}
