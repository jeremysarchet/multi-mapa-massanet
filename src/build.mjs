import marked from './scripts/marked.min.js';
import { load, loadAll } from './scripts/js-yaml.mjs';
import { readFileSync, writeFileSync } from 'fs';

import { CollectionPage } from './CollectionPage.mjs';
import { LandmarkPage } from './LandmarkPage.mjs';
import { meta } from './reusable.mjs';
import {
  BUCKET_BASE_URL,
  DATE_OPTIONS,
  createSlug,
} from './utils.mjs';

// TODO: Break out summary page rendering into its own module

export function parseMarkdown(text) {
  try {
    return marked.parse(text);
  } catch (error) {
    console.error('Problem parsing markdown', error, text);
    return text;
  }
}

let overview = '';
let details = '';

let totalCount = 0;
let totalNoLocationCount = 0;
let totalRoughLocationCount = 0;
let totalPreciseLocationCount = 0;
let totalHavingPhotosCount = 0;
let totalHavingDescriptionCount = 0;
let totalHavingLinksCount = 0;

const allAttachmentMetadata = load(readFileSync(`public/landmarks/attachments.yaml`));
const allPhotoMetadata = load(readFileSync(`public/landmarks/photos.yaml`));
const allCitations = load(readFileSync(`public/landmarks/citations.yaml`));

const entriesFromManuscript = loadAll(readFileSync(`public/landmarks/els-noms-toponyms.yaml`));
console.log('Remaining from manuscript:', entriesFromManuscript.length, '(Started with 1271)');

function landmarkCategory(thing, categorySlug, categoryDisplayName) {
  let noLocationCount = 0;
  let roughLocationCount = 0;
  let preciseLocationCount = 0;

  let count = 0;

  let detailsRows = '';

  const landmarks = loadAll(readFileSync(`public/landmarks/${thing}s.yaml`));
  landmarks.sort((a, b) =>
    (a.alpha ?? a.name).localeCompare((b.alpha ?? b.name))
  );

  for (const landmark of landmarks) {
    const slug = createSlug(landmark.name);

    // Check for duplicates
    if (landmarkSlugsSet.has(slug)) {
      console.warn(`Warning: landmark "${slug}" exists more than once.`);
    }
    landmarkSlugsSet.add(slug);

    let statusEmoji;
    let statusText;

    if (!(landmark.location || landmark.track)) {
      noLocationCount += 1;
      statusEmoji = '🔴';
      statusText = 'sense';
    } else if (landmark.location?.unsure || landmark.track?.unsure) {
      roughLocationCount += 1;
      statusEmoji = '🟡';
      statusText = 'aproximada';
    } else {
      preciseLocationCount += 1;
      statusEmoji = '🟢';
      statusText = 'precís';
    }

    const landmarkPageFilePath = `public/marcadors/${slug}.html`;
    const landmarkPage = LandmarkPage(
      landmark,
      slug,
      categorySlug,
      categoryDisplayName,
      statusEmoji,
      statusText,
      allAttachmentMetadata,
      allPhotoMetadata,
      allCitations,
    );

    try {
      writeFileSync(landmarkPageFilePath, landmarkPage);
    } catch (error) {
      console.error(`Problem writing to file ${landmarkPageFilePath}`, error);
    }

    const extraStatusEmojis = [];

    if (landmark.photos) {
      extraStatusEmojis.push('📷');
      totalHavingPhotosCount += 1;
    }
    if (landmark.description) {
      extraStatusEmojis.push('📄');
      totalHavingDescriptionCount += 1;
    }
    if (landmark.links) {
      extraStatusEmojis.push('🔗');
      totalHavingLinksCount += 1;
    }

    detailsRows += `<tr>
      <td>${statusEmoji}</td>
      <td>
        <a href="../marcadors/${slug}.html">
          ${landmark.name}
        </a>
        <div class="extra-status-indicators">${extraStatusEmojis.join(' ')}</div>
      </td>
    </tr>`;

    count += 1;
  }

  overview += `<tr>
    <td>
      <a href='#${categorySlug}'>
        ${categoryDisplayName}
      </a>
    </td>
    <td class='numeric'>${landmarks.length}</td>
    <td class='numeric'>${noLocationCount}</td>
    <td class='numeric'>${roughLocationCount}</td>
    <td class='numeric'>${preciseLocationCount}</td>
  </tr>`;

  details += `<div>
    <a class="back-to-top" href='#'>Inici</a>
    <h2 id='${categorySlug}'>
      ${categoryDisplayName} (${landmarks.length})
    </h2>
    <table class="group-index-table">
      ${detailsRows}
    </table>
  <div>`;

  totalCount += landmarks.length;
  totalNoLocationCount += noLocationCount;
  totalRoughLocationCount += roughLocationCount;
  totalPreciseLocationCount += preciseLocationCount;

  console.log(`Made pages for ${count} ${thing}s.`);
}

const landmarkSlugsSet = new Set();

landmarkCategory('fountain', 'fonts', 'Fonts');
landmarkCategory('farmhouse', 'masos', 'Masos');
landmarkCategory('nonextant-farmhouse', 'masos-desapareguts', 'Masos desapareguts i casals');
landmarkCategory('tree', 'arbres', 'Arbres');
landmarkCategory('bridge', 'ponts', 'Ponts');
landmarkCategory('house', 'cases', 'Cases');
landmarkCategory('saddle', 'collades', 'Collades, prats');
landmarkCategory('field', 'fields', "Camps, prats, boscos");
landmarkCategory('pool', 'gorgues', 'Gorgs, gorgues, cascades');
landmarkCategory('mine', 'mines', 'Mines, pedreres');
landmarkCategory('monument', 'monuments', 'Monuments');
landmarkCategory('peak', 'cims', 'Cims, muntanyes');
landmarkCategory('fort', 'llocs-defensius', 'Castells, forts');
landmarkCategory('mill', 'llocs-comercials', 'Comerç');
landmarkCategory('chapel', 'llocs-religiosos', 'Esglésies, capelles');
landmarkCategory('road', 'rutes', 'Camins, carreteres');
landmarkCategory('cave', 'coves', 'Coves');
landmarkCategory('stream', 'streams', 'Rieras, clots');
landmarkCategory('well', 'wells', "Pous, mines d'aigua, basses");

overview += `<tr>
  <td><b>Total</b></td>
  <td class='numeric'><b>${totalCount}</b></td>
  <td class='numeric'><b>${totalNoLocationCount}</b></td>
  <td class='numeric'><b>${totalRoughLocationCount}</b></td>
  <td class='numeric'><b>${totalPreciseLocationCount}</b></td>
</tr>`;

const summaryPageContent = `<html lang="ca">
<head>
  ${meta(
    'Resum dels marcadors | Multi mapa Maçanet',
    'Estat de geolocalització dels marcadors',
    'https://jeremysarchet.gitlab.io/multi-mapa-massanet/resum/',
    'https://jeremysarchet.gitlab.io/multi-mapa-massanet/images/2023-05-14-summary-screenshot.png',
    'png',
    630,
  )}

  <link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
  <link rel="stylesheet" type="text/css" href="./stats.css"/>
</head>
<body onLoad="init()">
  <a id="nav-back" href="..">Tornar al mapa</a>
  <h1>Resum dels marcadors</h1>
  <div id="overview">
    <table id="geolocation-table">
      <thead>
        <tr>
          <th rowspan="2" style="border-top: none; border-left: none"></th>
          <th rowspan="2">Total</th>
          <th colspan="3">Nivell de geolocalització</th>
        </tr>
        <tr>
          <th>🔴 sense</th>
          <th>🟡 aprox.</th>
          <th>🟢 precís</th>
        </tr>
      </thead>
      <tbody>
        ${overview}
      </tbody>
    </table>
    <table>
      <thead>
        <tr>
          <th colspan="2">Suplements</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>📄 Marcadors amb descripcions</td>
          <td>${totalHavingDescriptionCount}</td>
        </tr>
        <tr>
          <td>📷 Marcadors amb fotos</td>
          <td>${totalHavingPhotosCount}</td>
        </tr>
        <tr>
          <td>🔗 Marcadors amb enllaços</td>
          <td>${totalHavingLinksCount}</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div id="details">
    ${details}
  </div>
</body>
</html>
`;

const summaryPageFilePath = `public/resum/index.html`;
try {
  writeFileSync(summaryPageFilePath, summaryPageContent)
} catch (error) {
  console.error(`Problem writing to file ${summaryPageFilePath}`, error);
}

CollectionPage(
  'Mines per tipus de mineral',
  'public/landmarks/mines-per-tipus-de-mineral.yaml',
);
CollectionPage(
  'Cases per zona',
  'public/landmarks/cases-per-zona.yaml',
);
CollectionPage(
  'Gorgues per riera',
  'public/landmarks/gorgues-per-riera.yaml',
);
