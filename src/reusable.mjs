export function meta(
  title,
  description,
  socialPageURL,
  socialPhotoURL,
  imageType,
  imageWidth
) {
  return `<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Primary Meta Tags -->
    <meta name="title" content="${title}">
    <meta name="author" content="Comunitat de Maçanet" />
    <meta name="description" content="${description}">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="${socialPageURL}">
    <meta property="og:title" content="${title}">
    <meta property="og:description" content="${description}">
    <meta property="og:image" content="${socialPhotoURL}">
    <meta property=”og:image:width” content="${imageWidth ?? 600}">
    <meta property="og:image:type" content="image/${imageType ?? 'jpg'}">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="${socialPageURL}">
    <meta property="twitter:title" content="${title}">
    <meta property="twitter:description" content="${description}">
    <meta property="twitter:image" content="${socialPhotoURL}">`;
}
