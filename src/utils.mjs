export const DATE_OPTIONS = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  timeZone: "Europe/Madrid",
};

export const BUCKET_BASE_URL = 'https://toponims-de-massanet.s3.eu-west-3.amazonaws.com';

export function createSlug(title) {
  const trimmedTitle = title.trim();

  const diacriticsRemoved = trimmedTitle.normalize("NFD").replace(/[\u0300-\u036f]/g, '');

  const slug = diacriticsRemoved.replace(/[\ ']/g, '-').replace(/[^\w-]+/g, '').toLowerCase();

  return slug;
}

const urlMap = new Map([
  [
    'https://www.massanetdecabrenys.com/noms.html',
    'Els noms de Casa — massanetdecabrenys.com (2003)'
  ],
  [
    'https://www.massanetdecabrenys.com/masos.html',
    'Els Masos Del Terme — massanetdecabrenys.com (1989-90)'
  ],
  [
    'https://www.massanetdecabrenys.com/mines.html',
    'Mines i Pedreres — massanetdecabrenys.com (2000)'
  ],
  [
    'https://www.massanetdecabrenys.com/molins.html',
    'Els Molins Del Terme — massanetdecabrenys.com (1991)'
  ],
  [
    'https://www.massanetdecabrenys.com/castells.html',
    'Els Castells — massanetdecabrenys.com (2002)'
  ],
  [
    'https://www.massanetdecabrenys.com/gorgs.html',
    'Gorgs i Gorgues — massanetdecabrenys.com (2008)'
  ],
  [
    'https://www.massanetdecabrenys.com/lesfonts.html',
    'Les Fonts — massanetdecabrenys.com (1990)'
  ],
  [
    'https://www.massanetdecabrenys.com/arbres.html',
    'Els Arbres Singulars i Monumentals — massanetdecabrenys.com (2006-07)'
  ],
  [
    'https://www.massanetdecabrenys.com/llegendes.html',
    'Les Llegendes — massanetdecabrenys.com (2013)'
  ],
  [
    'https://www.massanetdecabrenys.com/camins.html',
    'Camins i carreteres — massanetdecabrenys.com (2003)'
  ],
  [
    'https://www.massanetdecabrenys.com/ferro.html',
    'La Indústria del Ferro — massanetdecabrenys.com (2001)'
  ],

  [
    '../els-noms-del-terme.html',
    'Els Noms del Terme de Maçanet de Cabrenys (2023)'
  ],
  [
    '../els-noms-del-terme.html#patrimoni',
    'El Patrimoni Arquitectònic — Els Noms del Terme de Maçanet de Cabrenys (2023)',
  ],

  [
    'https://maçanetdecabrenys.cat/esglesies-i-ermites/', 'Esglèsies i Ermites (Ajuntament de Maçanet de Cabrenys)'
  ],
]);

export function getTextForURL(url) {
  if (urlMap.has(url)) {
    return urlMap.get(url);
  } 
  return url;
}
