#!/bin/bash

# Run this with a path to a photo like so:
#
#   tools/add-photo.sh path/to/photo
#
#   or a URL and a key like so:
#
#   https://www.massanetdecabrenys.com/El%20Dav%C3%AD2.jpg www.massanetdecabrenys.com/El-Davi.jpg
#
# If a URL is provided, we'll download it first.

# Then, it will read metadata fields and output a YAML snippet with them.

# Then it will ask if you'd like to proceed to upload the photo to s3.
#
# Typical workflow:
#
# 1. Run this script
# 2. Notice some metadata fields aren't set, e.g. "author"
# 3. Set those fields like so:
#      exiftool -author="Jane Doe" <path to photo>
# 4. Re-run this script
# 5. Confirm the metadata fields look good
# 6. Confirm the photo key looks good
#      We want a key that works both as the S3 bucket object key, and works
#      as-is for the S3 object URL. This script simply replaces spaces with
#      hyphens, which may not always be good enough.
# 7. Type "y" to proceed with upload
# 8. Script will attempt to open the photo by its public URL in the browser to
#    confirm the upload worked.

BUCKET_NAME="toponims-de-massanet"

# Check if exiftool is installed
if ! command -v exiftool &> /dev/null; then
    echo "exiftool is not installed. Please install it before running this script."
    exit 1
fi

# Check if ImageMagick is installed
if ! command -v convert >/dev/null 2>&1; then
  echo "ImageMagick is not installed. Please install it first."
  exit 1
fi

# Check if aws is installed
if ! command -v aws >/dev/null 2>&1; then
  echo "AWS CLI is not installed. Please install it first."
  exit 1
fi

# Check if a photo argument is provided
if [ -z "$1" ]; then
    echo "Please provide the photo URL or local file path as an argument."
    exit 1
fi

# Check whether a URL or a local path was provided
if [[ "$1" =~ ^https?:// ]]; then
  echo 'Got a URL'

  # Check if a photo key argument is provided
  if [ -z "$2" ]; then
      echo "Please provide a photo key argument."
      exit 1
  fi


  clean_url="${1%%\?*}" # strip query parameters

  photo_key=$2

  photo=$(basename "$photo_key")

  # Check if the input image exists
  if [ -f "$photo" ]; then
    echo "Found file with matching basename. Must be already downloaded."
  else
    echo "Downloading..."
    curl -o $photo $1;
    exiftool -webstatement=$clean_url $photo
  fi
else
  # Check if the input image exists
  if [ ! -f "$1" ]; then
    echo "The provided image path does not exist."
    exit 1
  fi

  # Generate a key for the S3 object which also works as-is as the object URL
  # Should have no spaces or special characters to work as-is.
  photo_basename=$(basename "$1")
  photo_key=${photo_basename// /-}
  photo=$1
fi




# Run exiftool commands to extract metadata
if [ -n "$(exiftool -datetimeoriginal "$photo")" ]; then
  datetime_original=$(exiftool -datetimeoriginal -d "%Y-%m-%d %H:%M:%S" -T "$photo")

  if [ -n "$(exiftool -offsettimeoriginal "$photo")" ]; then
    offset_time_original=$(exiftool -offsettimeoriginal -T "$photo")
    datetime_original+=" $offset_time_original"
  fi
fi

if [ -n "$(exiftool -datecreated "$photo")" ]; then
  date_created=$(exiftool -datecreated -d "%Y-%m-%d %H:%M:%S" -T "$photo")
fi

if [ -n "$(exiftool -gpsposition "$photo")" ]; then
  gps_position_raw=$(exiftool -gpsposition -n -T "$photo")
  gps_position=$(awk '{ printf("[%s, %s]\n", $photo) }' <<< "$gps_position_raw")
fi

if [ -n "$(exiftool -gpsaltitude "$photo")" ]; then
  gps_altitude=$(exiftool -gpsaltitude -n -T "$photo")
fi

if [ -n "$(exiftool -make "$photo")" ]; then
  make=$(exiftool -make -T "$photo")
fi

if [ -n "$(exiftool -model "$photo")" ]; then
  model=$(exiftool -model -T "$photo")
fi

if [ -n "$(exiftool -author "$photo")" ]; then
  author=$(exiftool -author -T "$photo")
fi

if [ -n "$(exiftool -credit "$photo")" ]; then
  credit=$(exiftool -credit -T "$photo")
fi

if [ -n "$(exiftool -title "$photo")" ]; then
  title=$(exiftool -title -T "$photo")
fi

if [ -n "$(exiftool -pagename "$photo")" ]; then
  page_name=$(exiftool -pagename -T "$photo")
fi

if [ -n "$(exiftool -webstatement "$photo")" ]; then
  web_statement=$(exiftool -webstatement -T "$photo")
fi

if [ -n "$(exiftool -caption "$photo")" ]; then
  caption=$(exiftool -caption -T "$photo")
fi

# Echo the YAML block
echo "
Got the following metadata, formatted here in YAML:

$photo_key:
  datetime_original: $datetime_original
  date_created: $date_created
  gps_position: $gps_position
  gps_altitude: $gps_altitude
  make: $make
  model: $model
  author: $author
  credit: $credit
  title: $title
  page_name: $page_name
  web_statement: $web_statement
  caption: $caption
"

open "$photo"

# Prompt the user for confirmation
read -p "Does the metadata look good?

Upload to the S3 bucket with key: \"$photo_key\"? (y/n) " answer

# Check the user's response
if [[ "$answer" =~ ^[Yy]$ ]]; then
  echo "Uploading image..."
  aws s3 cp "$photo" "s3://$BUCKET_NAME/$photo_key"
  file "$photo"
  open "https://toponims-de-massanet.s3.eu-west-3.amazonaws.com/$photo_key"

  echo "Creating copy sized to 600px wide..."
  temp_image="resized_image.jpg"
  convert "$photo" -resize 600x "$temp_image"
  aws s3 cp "$temp_image" "s3://$BUCKET_NAME/600/$photo_key.600.jpg"
  rm "$temp_image"
  open "https://toponims-de-massanet.s3.eu-west-3.amazonaws.com/600/$photo_key.600.jpg"

else
  echo "Not uploading."
  exit 0
fi
