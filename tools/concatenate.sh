# Combines tiles into a single image

NUM_ROWS=7
NUM_COLS=7

WIDTH=1024
HEIGHT=1024

TOTAL_WIDTH=$((${WIDTH} * $NUM_ROWS))
TOTAL_HEIGHT=$((${HEIGHT} * $NUM_COLS))

INPUT_DIR='public/images/ceramic-tiles'
OUTPUT_DIR='public/images/ceramic-tiles'

OUTPUT_FILE="${OUTPUT_DIR}/combined.jpg"


convert \
  -size "${TOTAL_WIDTH}x${TOTAL_HEIGHT}" xc:transparent \
  -page "+$(($WIDTH * 0))+$(($HEIGHT * 0))" "${INPUT_DIR}/1-1.jpg" \
  -page "+$(($WIDTH * 1))+$(($HEIGHT * 0))" "${INPUT_DIR}/1-2.jpg" \
  -page "+$(($WIDTH * 2))+$(($HEIGHT * 0))" "${INPUT_DIR}/1-3.jpg" \
  -page "+$(($WIDTH * 3))+$(($HEIGHT * 0))" "${INPUT_DIR}/1-4.jpg" \
  -page "+$(($WIDTH * 4))+$(($HEIGHT * 0))" "${INPUT_DIR}/1-5.jpg" \
  -page "+$(($WIDTH * 5))+$(($HEIGHT * 0))" "${INPUT_DIR}/1-6.jpg" \
  -page "+$(($WIDTH * 6))+$(($HEIGHT * 0))" "${INPUT_DIR}/1-7.jpg" \
  \
  -page "+$(($WIDTH * 0))+$(($HEIGHT * 1))" "${INPUT_DIR}/2-1.jpg" \
  -page "+$(($WIDTH * 1))+$(($HEIGHT * 1))" "${INPUT_DIR}/2-2.jpg" \
  -page "+$(($WIDTH * 2))+$(($HEIGHT * 1))" "${INPUT_DIR}/2-3.jpg" \
  -page "+$(($WIDTH * 3))+$(($HEIGHT * 1))" "${INPUT_DIR}/2-4.jpg" \
  -page "+$(($WIDTH * 4))+$(($HEIGHT * 1))" "${INPUT_DIR}/2-5.jpg" \
  -page "+$(($WIDTH * 5))+$(($HEIGHT * 1))" "${INPUT_DIR}/2-6.jpg" \
  -page "+$(($WIDTH * 6))+$(($HEIGHT * 1))" "${INPUT_DIR}/2-7.jpg" \
  \
  -page "+$(($WIDTH * 0))+$(($HEIGHT * 2))" "${INPUT_DIR}/3-1.jpg" \
  -page "+$(($WIDTH * 1))+$(($HEIGHT * 2))" "${INPUT_DIR}/3-2.jpg" \
  -page "+$(($WIDTH * 2))+$(($HEIGHT * 2))" "${INPUT_DIR}/3-3.jpg" \
  -page "+$(($WIDTH * 3))+$(($HEIGHT * 2))" "${INPUT_DIR}/3-4.jpg" \
  -page "+$(($WIDTH * 4))+$(($HEIGHT * 2))" "${INPUT_DIR}/3-5.jpg" \
  -page "+$(($WIDTH * 5))+$(($HEIGHT * 2))" "${INPUT_DIR}/3-6.jpg" \
  -page "+$(($WIDTH * 6))+$(($HEIGHT * 2))" "${INPUT_DIR}/3-7.jpg" \
  \
  -page "+$(($WIDTH * 0))+$(($HEIGHT * 3))" "${INPUT_DIR}/4-1.jpg" \
  -page "+$(($WIDTH * 1))+$(($HEIGHT * 3))" "${INPUT_DIR}/4-2.jpg" \
  -page "+$(($WIDTH * 2))+$(($HEIGHT * 3))" "${INPUT_DIR}/4-3.jpg" \
  -page "+$(($WIDTH * 3))+$(($HEIGHT * 3))" "${INPUT_DIR}/4-4.jpg" \
  -page "+$(($WIDTH * 4))+$(($HEIGHT * 3))" "${INPUT_DIR}/4-5.jpg" \
  -page "+$(($WIDTH * 5))+$(($HEIGHT * 3))" "${INPUT_DIR}/4-6.jpg" \
  -page "+$(($WIDTH * 6))+$(($HEIGHT * 3))" "${INPUT_DIR}/4-7.jpg" \
  \
  -page "+$(($WIDTH * 0))+$(($HEIGHT * 4))" "${INPUT_DIR}/5-1.jpg" \
  -page "+$(($WIDTH * 1))+$(($HEIGHT * 4))" "${INPUT_DIR}/5-2.jpg" \
  -page "+$(($WIDTH * 2))+$(($HEIGHT * 4))" "${INPUT_DIR}/5-3.jpg" \
  -page "+$(($WIDTH * 3))+$(($HEIGHT * 4))" "${INPUT_DIR}/5-4.jpg" \
  -page "+$(($WIDTH * 4))+$(($HEIGHT * 4))" "${INPUT_DIR}/5-5.jpg" \
  -page "+$(($WIDTH * 5))+$(($HEIGHT * 4))" "${INPUT_DIR}/5-6.jpg" \
  -page "+$(($WIDTH * 6))+$(($HEIGHT * 4))" "${INPUT_DIR}/5-7.jpg" \
  \
  -page "+$(($WIDTH * 0))+$(($HEIGHT * 5))" "${INPUT_DIR}/6-1.jpg" \
  -page "+$(($WIDTH * 1))+$(($HEIGHT * 5))" "${INPUT_DIR}/6-2.jpg" \
  -page "+$(($WIDTH * 2))+$(($HEIGHT * 5))" "${INPUT_DIR}/6-3.jpg" \
  -page "+$(($WIDTH * 3))+$(($HEIGHT * 5))" "${INPUT_DIR}/6-4.jpg" \
  -page "+$(($WIDTH * 4))+$(($HEIGHT * 5))" "${INPUT_DIR}/6-5.jpg" \
  -page "+$(($WIDTH * 5))+$(($HEIGHT * 5))" "${INPUT_DIR}/6-6.jpg" \
  -page "+$(($WIDTH * 6))+$(($HEIGHT * 5))" "${INPUT_DIR}/6-7.jpg" \
  \
  -page "+$(($WIDTH * 0))+$(($HEIGHT * 6))" "${INPUT_DIR}/7-1.jpg" \
  -page "+$(($WIDTH * 1))+$(($HEIGHT * 6))" "${INPUT_DIR}/7-2.jpg" \
  -page "+$(($WIDTH * 2))+$(($HEIGHT * 6))" "${INPUT_DIR}/7-3.jpg" \
  -page "+$(($WIDTH * 3))+$(($HEIGHT * 6))" "${INPUT_DIR}/7-4.jpg" \
  -page "+$(($WIDTH * 4))+$(($HEIGHT * 6))" "${INPUT_DIR}/7-5.jpg" \
  -page "+$(($WIDTH * 5))+$(($HEIGHT * 6))" "${INPUT_DIR}/7-6.jpg" \
  -page "+$(($WIDTH * 6))+$(($HEIGHT * 6))" "${INPUT_DIR}/7-7.jpg" \
  \
  -layers flatten \
  $OUTPUT_FILE

open $OUTPUT_FILE
