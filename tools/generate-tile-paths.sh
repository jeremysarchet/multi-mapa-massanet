#!/bin/bash

# Run from the `public` dir

TILESET_DIR='tileset-topo-12-13'
OUTPUT_FILE='raster-tile-paths.js'

echo 'const RASTER_TILE_PATHS = [' > $OUTPUT_FILE

find $TILESET_DIR -type f | sed 's/^/\ \ "\.\//g' | sed 's/$/",/g' | sed '/DS_Store/d' | sort -n >> $OUTPUT_FILE

echo '];' >> $OUTPUT_FILE

echo "Wrote tiles paths to $OUTPUT_FILE"
