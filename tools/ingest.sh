#!/bin/bash

# Designed to take copy-pasted text from a page like: https://www.massanetdecabrenys.com/lesfonts.html
# and get it ready to put in the yaml landmark files.

# Watch out for:
#  - periods in the name
#  - multi-paragraph descriptions
sed -r 's|(^[^.]+)\:\ (.*$)|name: \1\ndescription:\n  credit: Pere Roura\n  value: >\n\ \ \ \ \2\n---|g' raw.txt | sed '/^$/d'
