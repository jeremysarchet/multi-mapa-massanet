echo -e "    mentions:\n$(
  sed 's/^\ *//g' |
  tr '\n' ' ' |
  sed 's/\.\ *El/.\nEl/g' |
  sed 's/^\(.*\)$/# \1\n\1/g' |
  sed 's/^El\ /- year: /g' |
  sed 's/\(year: [0-9]*\), /\1\n  quote: /' |
  sed '/^\s*[^#]/ s/\*//g' | # Only during conversion from "citations" to "mentions" schema
  sed '/^\s*[^#]/ s/(\([^(]*\))/\n  citation: \1\n/g' |
  grep -v '^\.\ *$' |
  sed 's/^/      /g' |
  sed 's/\ $//g' |
  grep -v '^\s*$'
)"
