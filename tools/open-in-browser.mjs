#!/usr/bin/env node

/**
 * Handy way, for development, to quickly open up a marker page at localhost.
 *
 * Takes in a place name e.g. "Collada Fonda" and computes the slug via our
 * `getSlug` util.
 * 
 * Then opens the URL in the browser, e.g.
 *   "http://localhost:8000/marcadors/collada-fonda.html"
 */

import { readFileSync } from 'fs';

import { createSlug } from '../src/utils.mjs';
import { exec } from 'child_process';

// Read from standard input, then trim and call getSlug
const input = readFileSync(0, 'utf-8').trim();
let placeName = input;

// If called e.g. with Visual mode in Vim:
// e.g. `:'<,'>w !tools/open-in-browser.mjs`
if (input.startsWith('name: ')) {
  placeName = placeName.split('name: ')[1];
}

const slug = createSlug(placeName);

console.log('Open in browser:', slug);

const url = `http://localhost:8000/marcadors/${slug}.html`;
exec(`open "${url}"`, (error) => {
  if (error) {
    console.error('Failed to open browser:', error);
  }
});
