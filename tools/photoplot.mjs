import { readFileSync, writeFile } from 'fs';

// Takes in a multi-line string with photo filenames and their coordinates, and
// outputs an HTML file with a Leaflet map with the photos added to the map
// as markers.

const outputFilePath = process.argv[2];
const photosWithLocations = process.argv[3].trim().split('\n');

let photos = `
           let popupContent = '';
`;

let popupContent = '';
for (const photo of photosWithLocations) {
  const [filename, location] = photo.split(": ");
  if (location === '[-, ]') {
    console.log('No location data for photo: ', filename);
    continue;
  }
  photos+=`
           popupContent = '<p>${filename}</p>';
           popupContent += '<image src="${filename}" width="300" />';
           L.marker(${location}).addTo(map).bindPopup(popupContent);
  `;
}

const page = `<html lang="ca">
<head>
    <title>Photoplot</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="ICGC" />
    <meta name="description" content="Contextmaps rasters Catalunya">
    <meta name="robots" content="index,follow">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <style>
        body {
            margin: 0;
        }
        #map {
            width: 100%;
            height: 100%;
            background-color: #ffffff
        }
    </style>
    <script>
        function init() {
            const map = L.map('map', {
                center: [42.386, 2.749],
                zoom: 14,
                attribution: 'Institut Cartogràfic i Geològic de Catalunya CC-BY-SA-3'
            });
            const topoMonICGCCache = L.tileLayer('https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: 'Institut Cartogràfic i Geològic de Catalunya CC-BY-SA-3'
            }).addTo(map);
            console.log(map);
            ${photos}
        }
    </script>
</head>
<body onLoad="init()">
    <div id="map"> </div>
</body>
</html>
`;

writeFile(
  outputFilePath,
  page,
  (err) => {
    if (err) {
      console.error(`Problem writing to file ${outputFilePath}`, err);
    }
});

console.log(`Wrote file: ${outputFilePath}`);
