#!/bin/bash

# Usage: `tools/plot-photos.sh <path to directory>

# Goes through photos in a directory, gets their coordinates with `exiftool`,
# then passes the filenames and coordinates to a Node script which builds an
# HTML file which is a page with Leaflet with the photos located on the map.

# Check if exiftool is installed
if ! command -v exiftool &> /dev/null; then
    echo "exiftool is not installed. Please install it before running this script."
    exit 1
fi

# Check if a photo filepath argument is provided
if [ -z "$1" ]; then
    echo "Please provide the directory path as an argument."
    exit 1
fi

# Check if the directory exists
if [ ! -d "$1" ]; then
  echo "Directory not found: $1"
  exit 1
fi

output_file="${1}/plot.html"

photos_with_locations=""

# Loop through all files with the ".jpg" extension
for file in "$1"/*.jpg; do
  # Check if any files match the pattern
  if [ -e "$file" ]; then
    # Get the filename and size on disk
    filename=$(basename "$file")
    size=$(du -h "$file" | awk '{print $1}')

    gps_position_raw=$(exiftool -gpsposition -n -T "$file")
    gps_position=$(awk '{ printf("[%s, %s]\n", $1, $2) }' <<< "$gps_position_raw")

    # Print the filename and size on disk
    photos_with_locations+="$filename: $gps_position"$'\n'

  fi
done

node tools/photoplot.mjs "$output_file" "$photos_with_locations"

open $output_file
