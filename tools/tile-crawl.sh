# https://github.com/stefanhuber/map-tiles-crawler
# npm i map-tiles-crawler (-g for global install)

mtc --url https://geoserveis.icgc.cat/icc_mapesmultibase/noutm/wmts/topo/GRID3857/{z}/{x}/{y}.png \
    --target ./tiles/ \
    --level 18 \
    --topLeft 42.48780:2.53681 \
    --bottomRight 42.29153:2.93884 \

# Map bounds
# --topLeft 42.48780:2.53681 \
# --bottomRight 42.29153:2.93884 \

# Ceramic bounds
# --topLeft 42.44613:2.64331 \
# --bottomRight 42.32262:2.81572 \
